# java面试宝典

## 前言

**创作不易，先star 再看，感谢！**

**此篇面试宝典的所有素材是由本人长时间通过网络技术博客、框架的官方文档、java相关的技术书籍等结合自己的理解整理出来的一篇比较全面的面试宝典，希望可以对需要面试的你，提供一丝微弱的帮助，由于本人不是全职编写，目前还在努力更新，您的一次star，就是我继续更新的动力。**

**本文设计的知识点包括但不限于：java基础（已完结）、多线程（更新中）、spring、springboot、redis缓存、mysql、mq、springcloud、数据结构于算法等。**

查阅书籍：《java并发编程的艺术》、《java并发编程之美》等等。




## java基础篇



### java面向对象四大特性

- 封装：

在面向对象编程方法中，封装（英语：Encapsulation）是指，一种将抽象性函数接口的实现细节部分包装、隐藏起来的方法。同时，它也是一种防止外界调用端，去访问对象内部实现细节的手段，这个手段是由编程语言本身来提供的。封装被视为是面向对象的四项原则之一。
适当的封装，可以将对象使用接口的程序实现部分隐藏起来，不让用户看到，同时确保用户无法任意更改对象内部的重要资料，若想接触资料只能通过公开接入方法（Publicly accessible methods）的方式（ 如："getters" 和"setters"）。它可以让代码更容易理解与维护，也加强了代码的安全性。

- 抽象

抽象是指在 OOP 中让一个类抽象的能力。一个抽象类是不能被实例化的。类的功能仍然存在，它的字段，方法和构造函数都以相同的方式进行访问。你只是不能创建一个抽象类的实例。

如果一个类是抽象的，即不能被实例化，这个类如果不是子类它将没有什么作用。这体现了在设计过程中抽象类是如何被提出的。一个父类包含子类的基本功能集合，但是父类是抽象的，不能自己去使用功能

- 继承

继承（英语：inheritance）是面向对象软件技术当中的一个概念。如果一个类别B“继承自”另一个类别A，就把这个B称为“A的子类”，而把A称为“B的父类别”也可以称“A是B的超类”。继承可以使得子类具有父类别的各种属性和方法，而不需要再次编写相同的代码。在令子类别继承父类别的同时，可以重新定义某些属性，并重写某些方法，即覆盖父类别的原有属性和方法，使其获得与父类别不同的功能。另外，为子类追加新的属性和方法也是常见的做法。 一般静态的面向对象编程语言，继承属于静态的，意即在子类的行为在编译期就已经决定，无法在运行期扩展。

有些编程语言支持多重继承，即一个子类可以同时有多个父类，比如C++编程语言；而在有些编程语言中，一个子类只能继承自一个父类，比如Java编程语言，这时可以透过实现接口来实现与多重继承相似的效果。

现今面向对象程序设计技巧中，继承并非以继承类别的“行为”为主，而是继承类别的“类型”，使得组件的类型一致。另外在设计模式中提到一个守则，“多用合成，少用继承”，此守则也是用来处理继承无法在运行期动态扩展行为的遗憾。

- 多态

指为不同数据类型的实体提供统一的接口，或使用一个单一的符号来表示多个不同的类型

**多态的最常见主要类别有：**

特设多态：为个体的特定类型的任意集合定义一个共同接口。
参数多态：指定一个或多个类型不靠名字而是靠可以标识任何类型的抽象符号。
子类型（也叫做子类型多态或包含多态）：个名字指称很多不同的类的实例，这些类有某个共同的超类。



### static关键字

 static 在 java 中是一个非常重要的关键字，主要用于修饰类、成员变量、成员方法，除此之外，我们还可以将代码块修饰为 static，被 static 修饰的方法或者变量不需要依赖对象的实例化，只要类被加载，被static修饰的方法或者属性就能被使用。

**static 修饰成员变量**

被static 修饰的成员变量被称为：**静态变量**。静态变量被所有对象共享，在内存中只有一个副本，在程序加载静态变量所属类的时候，静态变量就会被初始化。非静态变量是对象级别的，也就是说非静态变量的创建永远跟随着对象的实例，存在多个副本。

**static 修饰方法**

被 static 修饰的方法称为：**静态方法**。由于静态方法也不需要依赖对象，所以对于静态方法来说，是没有 this 的，并且静态变量不能访问非静态方法，原因很简单，静态方法的加载时间是跟随类的加载，而非静态方法需要对象被实例化之后才能被使用，会导致在静态方法里面根本就找不到你要调用的那么非静态方法。但是非静态方法是可以调用静态方法的，原因也很简单，**非静态方法存在的时候，静态方法一定存在，但是静态方法存在的时候非静态方法不一定存在**。

**static 修饰代码块**

被 static 修饰的代码块成为：**静态代码块**。静态代码块可以存在一个类的任何位置，跟随着类的加载被执行，一个类中可以存在多个静态代码块，他们严格的按照 stattic 的顺序来执行每个 静态代码块，并且每个 静态代码块只会被程序执行一次，由于这个特性，静态代码块用的好的话可以提升程序的性能。

**static 误区**

与 C/C++不同的是，java 中的static关键字不会影响到变量或者方法的作用域，只有 private、public、protected 可以影响 java 中变量或者方法的作用域。



### final关键字

final：最终的，通常表示这是无法修改的。一个属性在什么样的情况下才会被设计成无法修改呢？可能是因为**设计**和**效率**。

**final修饰的数据**：表示数据是永恒不变的，比如：

- 一个永远不改变的编译时常量。
- 一个在运行时被初始化的值，而程序员不希望它被修改。

编译时常量：编译器可以将该常量带入任何可能使用到它的计算中，可以在编译时执行计算，减轻了一些运行时的负担。需要注意的是，java在定义这些常量时必须是基本数据类型，并且以 final 关键字修饰，并且需要给定初始值。

一个既是 static 优势 final 修饰的域只占用一段不饿能该百年的存储空间。

当 final 修饰的不是基本数据类型，而是引用类型时（比如：对象），这个时候容易让人犯迷糊，**final 可以让基本数据类型的值永恒不变**，**final 可以让引用类型的引用永恒不变，但是引用对象的内容可以改变**（不能将一个数据的引用地址0x1 变成 0x2，但是可以改变0x1 对象中的属性，我可以将0x1对象的 name 属性改成任意值）。

**final 修饰类或方法**：

修饰类：表示这个类是永恒不变的，不可以被继承、不可以被覆盖，该类下的所有方法不能被重写，该类下的所有成员变量不能被覆盖。

修饰方法：继承类无法重写被 final 修饰的方法。

**private 与final** 

一个类中，所有的 private 方法或者属性都是被隐式的指定为 final，由于private 的访问权限，所以子类无法重写 private 的方法，我们可以对 private 方法添加 final 关键字，但是这并不能给该方法增加任何额外的意义，语法不会报错，但是这样写显得有点重复。

请注意，final 类禁止继承，所以 final 类下的所有方法都会被隐式的指定为 final ，无法重写他们。 在 final 类中可以给方法添加 final 关键字，但是没有什么意义。

**建议**

在设计类的时候，尽可能的将类设计成 final ，出于设计考虑，你希望不希望有人来修改这个类的任何东西，但是你无法预测是否有人会修改你的类，所以，如果你希望该类或者方法不被修改，那么最好将他设置为final 。



### String常用的方法有哪些

1. length()：返回字符串的长度。
2. isEmpty()：判断字符串的长度是否等于0（ value.length == 0）
3. charAt()：返回指定索引下的字符。
4. getBytes()：字符串转字节。
5. equals()：判断两个字符串的值是否相同
6. equalsIgnoreCase()：比较两个字符串是否相等，不区分大小写。
7. compareTo()：比较对应字符的大小(ASCII码顺序)，如果第一个字符和参数的第一个字符不等，结束比较，返回他们之间的长度**差值**，如果第一个字符和参数的第一个字符相等，则以第二个字符和参数的第二个字符做比较，以此类推,直至比较的字符或被比较的字符有一方结束。
8. startsWith()：检测字符串是否以指定的前缀开始。
9. endsWith()：检测字符串是否以指定的后缀结束。
10. hashCode()：返回字符串的hashCode。
11. indexOf()：返回指定字符在字符串中第一次出现处的索引，如果此字符串中没有这样的字符，则返回 -1。
12. lastIndexOf()：返回指定字符在此字符串中最后一次出现处的索引，如果此字符串中没有这样的字符，则返回 -1。
13. substring()：截取字符串。
14. concat()：字符串拼接。
15. replace()：字符串替换。
16. contains()：判断当前字符串是否包含某个字符。
17. split()：字符串分割，返回一个数组。
18. join()：将集合或数组通过指定字符分割组合成字符串。
19. toLowerCase()：将所有字符转换为小写。
20. toUpperCase()：将所有字符转换为大写。
21. trim()：去除字符串前后空格。
22. toCharArray()：将此字符串转换为新的字符数组。
23. format()：使用指定的格式字符串和参数返回格式化字符串。
24. valueOf()：返回参数的字符串表示形式。
25. intern()：该方法的作用是把字符串加载到常量池中（jdk1.6常量池位于方法区，jdk1.7以后常量池位于堆）



### == 和 equals 的区别是什么

java中数据结构分为**基本数据类型**和**引用类型**，他们在使用 == 、equals 时的含义也不相同，我们分别来看看他们在基本数据类型和引用类型中的具体表现吧。

**==**

- 基本数据类型：比较两个值是否相同。
- 引用类型：比较两个对象是否相同（比较两个对象的引用地址是否相同）。

**equal**

Object 类中的 equals 方法和 == 的效果是一样的，有些类（String、Integer等）重写了 Object  类中的 equals 方法，将他们用于值的比较。

我们先来看看 Object 类中的 equals 方法源码 

```
public boolean equals(Object obj) {
    return (this == obj);
}
```

我们再来看看 String类中的 equals 方法

```
public boolean equals(Object anObject) {
    if (this == anObject) {
        return true;
    }
    if (anObject instanceof String) {
        String anotherString = (String)anObject;
        int n = value.length;
        if (n == anotherString.value.length) {
            char v1[] = value;
            char v2[] = anotherString.value;
            int i = 0;
            while (n-- != 0) {
                if (v1[i] != v2[i])
                    return false;
                i++;
            }
            return true;
        }
    }
    return false;
}
```

所以当我们判断两个对象是否相同使用 **equals** 其实和使用 **==** 是一样的，因为对象没有重写 equals 方法（当然，我们也可以重写 equals 方法 来实现对比值是对象的属性是否相同来判断对象是否相同），而当我们使用new Stirng() 创建的字符串 通过 equals 方法进行比较时，他们对比的就是字符串的值，如果值相同就返回 true，不会对比内存地址，因为 String 重写 Object类中的 equals 方法。

### 重载和重写的区别？

重载：发生在同一个类中，方法名必须相同，实质表现就是多个具有不同的参数个数或者类型的同名函数(返回值类型可随意，不能以返回类型作为重载函数的区分标准)，返回值类型、访问修饰符可以不同，发生在编译时。

重写： 发生在父子类中，方法名、参数列表必须相同，是父类与子类之间的多态性，实质是对父类的函数进行重新定义。返回值范围小于等于父类，抛出的异常范围小于等于父类，访问修饰符范围大于等于父类；如果父类方法访问修饰符为 private 则子类就不能重写该方法。



### 修饰符public、private、protected，以及不写（默认）时的区别？

**private**：私有，只有当前类有操作权。

**default**：当成员变量没有访问修饰符时默认为default，对于同一个包下，它相当于 public（公开），不同包下相当于 private （私有）。

**protected**：受保护，对同一个包中的类或者子类相当于 public（公开），对于不同包下且没有父子关系的相当于 private（私有）。

**public**：公开，对所有类都是公开的。

|    修饰符     | 当前类 | 同包 | 子类 | 其他包 |
| :-----------: | :----: | :--: | :--: | :----: |
|  **private**  |   ✔    |  ✖   |  ✖   |   ✖    |
|  **default**  |   ✔    |  ✔   |  ✖   |   ✖    |
| **protected** |   ✔    |  ✔   |  ✔   |   ✖    |
|  **public**   |   ✔    |  ✔   |  ✔   |   ✔    |

**注意**：

可以修饰外部类的修饰符只有 public 和 default。

public 修饰外部类时，在同一包内，可以访问，无需导包；同一包外可以访问，需要导包。

default 修饰外部类时，在同一包内，可以访问，无需导包；同一包外，无法访问。



### 构造器Constructor是否可被override?

构造器Constructor不能被继承，因此不能重写Override，但可以被重载Overload。

Constructor不能被继承，所以Constructor也就不能被override。每一个类必须有自己的构造函数，负责构造自己这部分的构造。子类不会覆盖父类的构造函数，相反必须负责在一开始调用父类的构造函数。



### String、StringBuffer、StringBuilder的区别

**String**：字符串常量，字符串长度不可变。Java 中 String 是 immutable（不可变）的。

**StringBuffer**：字符串变量（Synchronized，即线程安全），线程安全的可变字符序列，可以改变字符序列的长度和内容，如果需要频繁的对字符串进行拼接，StringBuffer 相对于  “+” 的重载效率会高得多，如果需要将 StringBuffer 转 String，只需要调用 StringBuffer 的toString() 方法。

**StringBuilder**：字符串变量（非线程安全，JDK1.5 引入）。在内部，StringBuilder 对象被当作是一个包含字符序列的变长数组。此类提供一个与 StringBuffer 兼容的 API，但不保证同步。该类被设计用作 StringBuffer 的一个简易替换，用在字符串缓冲区被单个线程使用的时候（这种情况很普遍）。

**三者区别**

String 和 StringBuffer区别主要在性能方面，由于 String 是不可变对象，所以通过 “+” 来拼接的字符串都会生成一个新的 String 类，然后将指针指向新的 String 对象。这**并不能说明 “+” 的性能就弱于StringBuffer**，我们知道，在 JDK 1.5 之后引入了 StringBuilder， “+”  是通过 StringBuilder 实现，**对于一般的字符串拼接，“+” 和 StringBuilder 的性能其实没有什么差别**，因为 **JVM 会对 String 的 “+” 做优化**，既然这样的话，是否就没有必要使用 StringBuffer 和 StringBuilder  了呢？不是的，如果你在某个循环中对字符串进行拼接， “+” 会在循环内构造 StringBuilder  ，也就是说，循环了多少次，StringBuilder  就被创建了多少次。而 StringBuffer 和 StringBuilder  在循环拼接的时候只需要创建一个对象即可，性能非常的明显。

对于 StringBuffer 和 StringBuilder ，他们都是 AbstractStringBuilder 抽象类的子类，都有着相同的实现，主要区别在于 StringBuffer 线程安全，而 StringBuilder  非线程安全，这也必然会导致 StringBuilder 的性能高于 StringBuffer 。

最后 String、StringBuffer、StringBuilder 的性能由高到低：StringBuilder  > StringBuffer > String。

![](https://p6.toutiaoimg.com/origin/pgc-image/4179ae335c4a469c8578241d54f28b73)

<center>
    <div style="color:orange; border-bottom: 1px solid #d9d9d9;
    display: inline-block;
    color: gray;
    padding: 2px;"><b>图 1-1</b></div>
    <br>
</center>

![](https://p6.toutiaoimg.com/origin/pgc-image/99d25617d070447cb27a3b6d4a67bd01)

<center>
    <div style="color:orange; border-bottom: 1px solid #d9d9d9;
    display: inline-block;
    color: gray;
    padding: 2px;"><b>图 1-2</b></div>
    <br>
</center>

![](https://p6.toutiaoimg.com/origin/pgc-image/f5c67ca0a1ba44518dea52e3b5741e4c)

<center>
    <div style="color:orange; border-bottom: 1px solid #d9d9d9;
    display: inline-block;
    color: gray;
    padding: 2px;"><b>图 1-3</b></div>
    <br>
</center>

![](https://p6.toutiaoimg.com/origin/pgc-image/e3bc3c637e8f4daa9655b9f51df086fb)

<center>
    <div style="color:orange; border-bottom: 1px solid #d9d9d9;
    display: inline-block;
    color: gray;
    padding: 2px;"><b>图 1-4</b></div>
    <br>
</center>

###  final, finally, finalize的区别

**final**：用于声明属性，方法和类，分别表示属性不可交变，方法不可覆盖，类不可继承。请参考前文中的**final关键字**的介绍。

**finally**：Java 中的 Finally 关键一般与try一起使用，在程序进入try块之后，无论程序是因为异常而终止或其它方式返回终止的，finally块的内容一定会被执行 。

**finalize**：是Object类的一个方法，在垃圾收集器执行的时候会调用被回收对象的此方法，供垃圾收集时的其他资源回收，例如关闭文件等。

通常来说，这三个关键字之前没有什么必然的联系，只是名字有点像，给人感觉他们很相似。



### 字节流与字符流的区别

字节流就是普通的二进制流，读出来的是bit。

字符流就是在字节流的基础按照字符编码处理，处理的是char。



###  什么是java序列化，如何实现java序列化？或者请解释Serializable接口的作用

Java 提供了一种对象序列化的机制，该机制中，一个对象可以被表示为一个字节序列，该字节序列包括该对象的数据、有关对象的类型的信息和存储在对象中数据的类型。

将序列化对象写入文件之后，可以从文件中读取出来，并且对它进行反序列化，也就是说，对象的类型信息、对象的数据，还有对象中的数据类型可以用来在内存中新建对象。

简单来说：**序列化就是将java对象转成字节流的过程，反序列化则是将字节流转成java对象的过程。**

**java实现序列化**

- **原生序列化**：java对象实现 Serializable 接口，通过原生流( InputStream/outputStream) 实现。
- **JSON序列化**：spring提供了一种默认的json序列化工具包：jackson ，通过ObjectMapper类来实现对象转byte[] 数组或者 json 串转对象的过程。
- **FastJson**：阿里巴巴旗下一个员工因为没有好用的序列化工具开发的，虽然目前该工具 bug 满天飞，但是使用 FastJson 的公司还是不少。
- **ProtoBuff序列化**： protobuf(Google Protocol Buffers)是Google提供一个具有高效的协议数据交换格式工具库(类似Json)，但相比于Json，Protobuf有更高的转化效率，时间效率和空间效率都是JSON的3-5倍。后面将会有简单的demo对于这两种格式的数据转化效率的对比。



### abstract class和interface有什么区别?

**abstract class**：含有 abstract 修饰符的class即为抽象类，abstract 类不能创建实例对象，含有 abstract 修饰的方法的类必须是 abstract 类，但是 abstract 类并不要求其所有方法都是抽象方法。抽象类中定义的抽象方法必须在继承的子类中实现，所以，不能有抽象构造方法或者抽象静态方法。如果子类没有实现父类（抽象类）的所有抽象方法，那么该子类也必须定义为抽象类。

**interface**：可以说成是抽象类的一种特例，接口中的所有方法都必须是抽象的。接口中的方法定义默认为public abstract类型，接口中的成员变量类型默认为public static final。

|          | abstract class（抽象类）                                 | interface（接口）                                            |
| -------- | -------------------------------------------------------- | ------------------------------------------------------------ |
| 类       | 继承关系，只能继承一个，但是可以实现多个接口             | 可以实现多个接口                                             |
| 数据     | 可以自定义                                               | 只能是静态的                                                 |
| 方法     | 私有或者受保护的，非抽象方法，但是抽象方法必须实现       | 只能是公开（public）方法                                     |
| 实例化   | 不能                                                     | 不能                                                         |
| 变量     | 可以定义私有变量、默认，可以在子类中重新定义或者重新赋值 | 只能是公开静态抽象变量（public static abstract ），实现类不能修改 |
| 设计     | is-a                                                     | like-a                                                       |
| 实现方式 | 继承（extends）                                          | 实现（implements）                                           |



## a=a+b与a+=b有什么区别吗?

+= 操作符会进行隐式自动类型转换,此处 a+=b 隐式的将加操作的结果类型强制转换为持有结果的类型,而 a=a+b 则不会自动进行类型转换。

![](https://p6.toutiaoimg.com/origin/pgc-image/cf4a526186d94db39cb75915b2e77e1a)

<center>
    <div style="color:orange; border-bottom: 1px solid #d9d9d9;
    display: inline-block;
    color: gray;
    padding: 2px;"><b>图 1-5</b></div>
    <br>
</center>

从图片中我们可以看出： a+b 的结果应该是 int类型，第一组操作 a += b;之所以没有报错是因为编译器给我自动做了类型转换（将int 类型强转为 short 类型），而  a=a+b 没有做类型转换，所以编译无法通过。

![](https://p5.toutiaoimg.com/origin/pgc-image/4215da5b52134641a0a0450e95c6e0bf)

<center>
    <div style="color:orange; border-bottom: 1px solid #d9d9d9;
    display: inline-block;
    color: gray;
    padding: 2px;"><b>图 1-6</b></div>
    <br>
</center>



### 一个类能拥有多个main方法吗？

可以，但只能拥有一个这样的main方法

```java
public static void main(String[] args) {


}
```

否者程编译将无法通过，警告你的main方法已存在。



### java支持什么类型的参数传递？

直接说结论吧，java只有**值传递**，没有引用传递，解释的话有点长，感兴趣的可以参考一下我之前写的文章：**[天真，居然还有人认为java的参数传递方式是引用传递](https://blog.csdn.net/qq_33220089/article/details/107084395)**。

这里说一下什么是引用传递、什么是值传递

**什么是引用传递？**
在C++中，函数参数的传递方式有引用传递。所谓引用传递是指在调用函数时将实际参数的地址传递到函数中，那么在函数中对参数所进行的修改，将影响到实际参数。

**什么是值传递？**
值传递是指在调用函数时将实际参数复制一份传递到函数中，这样在函数中如果对参数进行修改，将不会影响到实际参数。



### 集合

#### Arraylist 和 LinkedList的区别？

**arrayList**
ArrayList 是集合框架的一部分，存在于 java.util 包中。它为我们提供了 Java 中的动态数组。
虽然，它可能比标准数组慢，但在需要对数组进行大量操作的程序中很有帮助。此类位于java.util包中。
**特性**

- ArrayList 容器大小可以默认（10）也可以指定，如果数据增多或者减少，ArrayList 容器也会触发扩容或者缩减操作(ArrayList 的扩容通过数据拷贝的方式实现，浅拷贝，所以我们在使用 ArrayList 的时候最好指明容器大小，如果触发扩容次数较多，会严重影响性能)。
- ArrayList 支持**随机访问（下标访问）**，且效率非常高，时间复杂度：O(1)，但是按需查找（查找值），效率和链表一样，时间复杂度为：O(n)。
- ArrayList 只能被用于包装类型，基本类型不可用。
- **ArrayList 线程不安全的**，也就是不同步，可以使用 Vector代替。

**ArrayList 工作流程**：ArrayList通过构造函数创建了一个容器（默认容器大小 10），调用 add() 方法可以往 ArrayList 中的 elementData 数组中添加数据，
当添加的数据到达10个的时候，就会触发扩容操作，扩容一般是将容器扩大到原来的1.5倍（自己指定扩容的除外，手动扩容会将容器大小变成你指定的大小），
将老的数组拷贝到新数组中，然后废弃老数组。删除数据时，会将数组中指定的元素置空，然后做判断，如果删除的元素不是在最后一个，那么执行一遍数据拷贝，新数组的大小 -1。

![图 1-7](https://p6.toutiaoimg.com/origin/pgc-image/84a80e4849bf401db0d77ba7e87ebb61)

<center>
    <div style="color:orange; border-bottom: 1px solid #d9d9d9;
    display: inline-block;
    color: gray;
    padding: 2px;"><b>图 1-7</b></div>
    <br>
</center>

**LinkedList**
链表是一种线性数据结构，其中元素不存储在连续的内存位置。链表中的元素使用指针链接，链表由节点组成，每个节点中至少包含本身数据和指向下一个节点的指针。

**单向链表**

![image-20210812221105161](https://p9.toutiaoimg.com/origin/pgc-image/6c4c3fc3815c4cf38bb0440802caeb12)

<center>
    <div style="color:orange; border-bottom: 1px solid #d9d9d9;
    display: inline-block;
    color: gray;
    padding: 2px;"><b>图 1-8</b></div>
    <br>
</center>

**双向链表**

双链表是链表的一种，由节点组成，每个数据结点中都有两个指针，分别指向直接后继和直接前驱。



![image-20210812221105161](https://p5.toutiaoimg.com/origin/pgc-image/aec3382b7f9f41d3a6ec9fc407292f8f)

<center>
    <div style="color:orange; border-bottom: 1px solid #d9d9d9;
    display: inline-block;
    color: gray;
    padding: 2px;"><b>图 1-9</b></div>
    <br>
</center>



**LinkedList 特点**

- LinkedList是双向链表实现的List（双向链表指的是每个节点都会记录前一个节点和下一个节点的指针）。
- LinkedList 非线程安全。
- LinkedList元素允许为null，允许重复元素
- 插入删除效率高，时间复杂度：O(1)，查找效率一般，时间复杂度：O(n)。
- 不存在扩容的说法，添加数据直接在尾节点后添加，删除直接将上一个节点的下一个节点指针指向删除节点下一个节点的地址（有点绕。可以仔细品一下）。



**ArrayList 和 LinkedList的区别**

- 数据结构不同，**ArrayList 基于数组**；LinkedList 基于链表。
- 效率不同：**随机访问 ArrayList 快**，**增加删除 LinkedList  快**。
- 限制不同：ArrayList 由于基于数组，所以需要动态扩容，LinkedList 基于链表，不存在扩容的说法。
- 占用空间不同：ArrayList 需要申请一块连续的内存地址（数组的原因），LinkedList  对内存连续性没有要求（链表的原因）。





#### HashMap、HashSet、HashTable的区别?

**HashMap**

HashMap 是一个散列表，它存储的内容是键值对(key-value)映射，数据结构主要是：数组+链表的方式存储，jdk1.8 之后引入了红黑树用来优化链表过长问题。

HashMap 实现了 Map 接口，根据键的 HashCode 值存储数据，具有很快的访问速度，最多允许一条记录的键为 null，不支持线程同步。

HashMap 是无序的，即不会记录插入的顺序。

HashMap 继承于AbstractMap，实现了 Map、Cloneable、java.io.Serializable 接口。



**HashSet**

HashSet 实现了 Set 接口，从他的构造函数中可以得知他也是一个 HashMap，所以他也是**无序**的，但是它允许空元素，虽然多个null不会报错，但是由于set的重复判断的条件，HashSet 集合中只会出现一条 null 数据，其他null会被视为重复数据，同时，由于 HashSet 依赖HashMap实现，所以它也是**线程不安全**的。我们再来看看 HashSet 的几个重要特性。

- 实现了集合接口
- 由于 HashSet 实现了 Set 接口，所以 HashSet  **不允许出现重复数据**。
- HashSet 插入的对象无法保证以相同的顺序插入，**插入位置由 HashCode 决定**。
- HashSet 允许出现 **null 值**。
- HashSet 还实现了**Serializable**和**Cloneable**接口。



**HashTable**

HashTable 与HashMap 类似，它们存储的内容都是键值对(key-value)映射，不过 HashTable 不允许将空值作为key或者value，从哈希表中查询或者存储对象时，用作键的对象必须实现 hashCode() 方法和 equails()方法。由于HasTable 的添加操作在方法上加上了 synchronized 同步锁，所以他是线程安全的。HashTable 特点如下

与HashMap相似，但它线程安全。

Hashtable 将键/值对存储在哈希表中。

HashTable 类的初始默认容器是11（HashMap 初始容器为 16 ），loadFactor（装载因子） 0.75。

Hashtable 提供非快速失败的枚举，而HashMap没有。

**三者区别**

线程安全性：HashTable 由于加了 synchronized 同步锁，所以他是**线程安全**的，HashMap与 HashSet 则是**非线程安全**的。

数据重复：HashSet 由于实现了 Set接口，所以它不**允许出现重复的值**，HashMap中**不允许出现重复的键**，但是可以出现**重复的值**，HashTable也是如此，并且HashTable还不**允许将空值作为key或者value**。



#### HashMap是线程安全的吗？有什么替代集合吗？

**HashMap不是线程安全的**

**替代品**：

**HashTable**：通过在方法上添加 synchronized 同步锁来保证线程安全，效率偏低。

**Collections.synchronizedMap(Map)**：通过对象排斥锁 mutex 保证线程安全，效率偏低。

**ConcurrentHashMap**：CAS +分段锁的机制保证线程安全，相比前面两种，它的效率会好很多。



#### HashMap如何解决哈希冲突问题？

 **hash算法**

哈希算法是将任意长度的二进制值映射为较短的固定长度的二进制值，这个小的二进制值称为哈希值（java 通过 hash 将会得到一个整数 ）。

哈希值是一段数据唯一且极其紧凑的数值表示形式。如果散列一段明文而且哪怕只更改该段落的一个字母，随后的哈希都将产生不同的值。要找到散列为同一个值的两个不同的输入，在计算上是不可能的，所以数据的哈希值可以检验数据的完整性。一般用于快速查找和加密算法。

**hash碰撞**

Hash算法并不完美，有可能两个不同的原始值在经过哈希运算后得到同样的结果， 这样就是**哈希碰撞**。

我们希望的 hash：

- 散列函数计算得到的散列值是一个非负整数。

- 如果 key1 = key2，那 hash(key1) == hash(key2)。
- 如果 key1 ≠ key2，那 hash(key1) ≠ hash(key2)。

前面两点没啥说的，key 相同，hash计算出来的值必须一致，但是至于最后一点，想要找到一个不同 key 对应散列值（hash值）也都不一样的散列（hash）函数，那是几乎不可能的。即便像业界著名的MD5、SHA、CRC等哈希算法，也无法完全避免这种**散列冲突**。而且，因为数组的存储空间有限，也会加大散列冲突的概率。

**解决方案**

**开放寻址法**

如果出现了 hash 冲突，那我们就重新探测一个空闲位置，然后再插入，二如何重新探测新的插入位置呢？**线性探测**就能解决这个问题。线性探测指的是：如果某个 key 经过 散列函数得到的下标已经被占用了，这个时候就从当前位置出发，后移一位，判断位置是否空闲，空闲则插入，否则继续重复探测。

![img](https://static001.geekbang.org/resource/image/5c/d5/5c31a3127cbc00f0c63409bbe1fbd0d5.jpg)

<center>
    <div style="color:orange; border-bottom: 1px solid #d9d9d9;
    display: inline-block;
    color: gray;
    padding: 2px;"><b>图 1-10</b></div>
    <br>
</center>

如图所示：黄色的色块表示空闲位置，橙色的色块表示已经存储了数据。

但是这里存在一个问题，需要注意，有可能会被面试官挖坑哦。存储已经说完了，现在说说查找，查找和插入有点类似，我们通过散列函数求出需要查找元素的键值对应的散列值。然后比较数组中下标与计算出来下标的元素是否相同，如果相同，说明找到了，如果不相同，就会往后顺序一次查找，直到**查找到数组中的空闲位置，说明查找的元素并不存在当前数组中**。这点很重要，一定要牢记。这本身并没有什么问题，但是结合元素的删除，问题就出现了，**我们不能直接将需要删除的元素置空**，为什么呢？我们在查找时，如果通过线性探索的方式找到一个空闲的位置，那么就代表查找的元素不在数组中，假设数组中，下标 0、1、2分别村的数据是：zhangsan、lisi、wangwu、，这个时候我们将下标 1 质控，下标 2 计算出来的散列值原本是 0 ，通过线性探索 存放在 2 的下标，这个时候我们查找wangwu 的时候找到下标 为0 的元素，发现不相等，这个时候就会往后查找，但不巧的是，下标 1 被你置空了，所以查询结束，wangwu 不在当前数组中。这个坑一定要记住哦，解决方案很简单，将删除的元素做一个标识即可。但是线性探测存在比较大问题，当散列表中的数据越来越多时，发生散列冲突的概率就会越高，空闲的位置越来越少，极端情况下，需要扫描整个三散列表，所以先行**探索的最坏时间复杂度为：O(n)**。可以考虑另外两种探索方案：二次探索、双重探索（感兴趣的自己百度）。



**链表法**

目前 HashMap 解决散列冲突使用的就是链表法。相比于开放寻址法，链表法简单得多，只需要引用数组即可。在散列表中，每个桶或者槽都会对应一条链表，将发生散列冲突的元素存储到链表中即可。

![img](https://static001.geekbang.org/resource/image/a4/7f/a4b77d593e4cb76acb2b0689294ec17f.jpg)

<center>
    <div style="color:orange; border-bottom: 1px solid #d9d9d9;
    display: inline-block;
    color: gray;
    padding: 2px;"><b>图 1-11</b></div>
    <br>
</center>

插入：通过散列函数计算出散列值，如果发现当前下标已经存在元素（hash碰撞），这个时候将插入的元素插入到当前下标对应的链表中即可，效率非常的高：**O(1)**。插入的效率虽然很高，那删除和查询的效率如何？

查找、删除也需要计算出当前元素的散列值，如果和当前下标存储的元素不同，则需要遍历与之对应的整条链表。遍历链表的时间复杂度与链表的长度 k成正比，也就是 **O(k)** ，对于散列比较均匀的散列函数来说，理论上讲，k=n/m，其中 n 表示散列中数据的个数，m 表示散列表中“槽”的个数。

这里又存在一个问题，当hash碰撞过于频繁，导致某条链表过长，这个时候查找、删除的时间复杂度就是直线上升，官方人员也发现了这个问题，所以在 **JDK 1.8 的时候引入了红黑树**来防止链表过长导致效率降低问题。



### 为什么重写equals时必须重写hashCode方法？

我们经常听到的一句话：重写对象 equals() 方法时，必须也要重写hashCode() 方法，主要原因是什么请看下面介绍。

先说几个概念：

- **两个对象相等，hashCode一定相同。**
- **两个对象不等，hashCode 不一定不相同。**
- **hashCode 相同，两个对象不一定相等。**
- **hashCode 不同，两个对象一定不等。**

为什么会出现上面这四句话呢？首先我们要知道什么是 hashCode，hashCode 主要用于计算字符串的哈希码，什么？你问我什么是哈希码？请自行百度，我们以HashMap为例，get() /containsKey() 都会存在一个 hash(key) 这样的方法，返回一个整形数字，这个方法首先会计算出HashMap 中 key 的哈希值，然后在计算出当前 key 存储的数组下标地址，这样HashMap 就能快速的查询出当前 key 的数据或者校验当前key是否存在，之所以这样能查出来是因为存的时候也是按照这种方式计算的，比我们遍历HashMap取值快得多，计算一个 key 算出的哈希值是唯一的，那为什么还会出现上面的几种概念呢？那是因为不同的 key 算出来的哈希值有可能是一样的，我们还是以HashMap为例， 第一个 key 通过计算 得到的 下标为 6 ，第二个 key 通过计算得到的下标也是 6，这个时候就出现了**哈希碰撞**，HashMap通过引入**链表**的方式进行处理，所以这里可以看出 hashCode 虽然相同，但是 key 却不一定相同，理解了这个概念，其他三个概念就不难理解了，这下知道为什么重写 equals 时必须重写 hashCode 方法了吧。我在附上String类中 hashCode() 和 HashMap 的 hash() 方法源码。 

```
static final int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }
```

```
public int hashCode() {
        int h = hash;
        if (h == 0 && value.length > 0) {
            char val[] = value;

            for (int i = 0; i < value.length; i++) {
                h = 31 * h + val[i];
            }
            hash = h;
        }
        return h;
    }
```



## 多线程进阶篇

### 什么是进程、线程，他们有什么区别？

**进程**

狭义定义：进程是正在运行的程序的实例（an instance of a computer program that is being executed）。
广义定义：进程是一个具有一定独立功能的程序关于某个数据集合的一次运行活动。它是操作系统动态执行的基本单元，在传统的操作系统中，进程既是基本的分配单元，也是基本的执行单元。

进程的概念主要有两点：第一，进程是一个实体。每一个进程都有它自己的地址空间，一般情况下，包括文本区域（text region）、数据区域（data region）和堆栈（stack region）。文本区域存储处理器执行的代码；数据区域存储变量和进程执行期间使用的动态分配的内存；堆栈区域存储着活动过程调用的指令和本地变量。第二，进程是一个“执行中的程序”。程序是一个没有生命的实体，只有处理器赋予程序生命时（操作系统执行之），它才能成为一个活动的实体，我们称其为进程。 

进程是操作系统中最基本、重要的概念。是多道程序系统出现后，为了刻画系统内部出现的动态情况，描述系统内部各道程序的活动规律引进的一个概念,所有多道程序设计操作系统都建立在进程的基础上（百度百科）。

**进程是内存中一块独立的空间。**

**线程**

（英语：thread）是操作系统能够进行运算调度的最小单位。大部分情况下，它被包含在进程之中，是进程中的实际运作单位。一条线程指的是进程中一个单一顺序的控制流，一个进程中可以并发多个线程，每条线程并行执行不同的任务。在Unix System V及 SunOS 中也被称为轻量进程（lightweight processes），但轻量进程更多指内核线程（kernel thread），而把用户线程（user thread）称为线程。

线程是独立调度和分派的基本单位。线程可以为操作系统内核调度的内核线程，如Win32线程；由用户进程自行调度的用户线程，如 Linux 平台的POSIX Thread；或者由内核与用户进程，如Windows 7的线程，进行混合调度。

同一进程中的多条线程将共享该进程中的全部系统资源，如虚拟地址空间，文件描述符和信号处理等等。但同一进程中的多个线程有各自的调用栈（call stack），自己的寄存器环境（register context），自己的线程本地存储（thread-local storage）。

一个进程可以有很多线程来处理，每条线程并行执行不同的任务。如果进程要完成的任务很多，这样需很多线程，也要调用很多核心，在多核或多 CPU，或支持Hyper-threading 的 CPU 上使用多线程程序设计的好处是显而易见的，即提高了程序的执行吞吐率。以人工作的样子想像，核心相当于人，人越多则能同时处理的事情越多，而线程相当于手，手越多则工作效率越高。在单CPU单核的计算机上，使用多线程技术，也可以把进程中负责 I/O 处理、人机交互而常被阻塞的部分与密集计算的部分分开来执行，编写专门的 workhorse 线程执行密集计算，虽然多任务比不上多核，但因为具备多线程的能力，从而提高了程序的执行效率（维基百科）。

**线程是进程的最小运行单位，依赖于进程。**



### 同步与异步有什么区别？

**同步**：（英语：Synchronization），指对在一个系统中所发生的事件（event）之间进行协调，在时间上出现一致性与统一化的现象。在系统中进行同步，也被称为及时（in time）、同步化的（synchronous、in sync）。

**线程同步**：当有一个线程正在对内存某个属性进行操作时，其他线程需处于阻塞状态，直到当前线程执行结束，其他线程才能竞争内存中的属性。这就是线程同步。

 

**异步**：异步双方不需要共同的时钟，也就是接收方不知道发送方什么时候发送，所以在发送的信息中就要有提示接收方开始接收的信息，如开始位，同时在结束时有停止位。



### 什么是上下文切换

上下文切换指的是内核（操作系统的核心）在CPU上对进程或者线程进行切换。上下文切换过程中的信息被保存在**进程控制块**（PCB-Process Control Block）中。PCB又被称作**切换帧**（SwitchFrame）。上下文切换的信息会一直被保存在CPU的内存中，直到被再次使用。

上下文切换包括了很多切换，主要分为以下几种

- **线程切换**：同一进程中两个或以上线程之间进行切换。
- **进程切换**：两个或以上进程进行切换。
- **模式切换**：在给定线程中，用户模式与内核模式之间的切换。
- **地址空间切换**：将虚拟内存切换到物理内存。

**为什么需要线程、进程切换？**

为了提高cpu的利用效率，以及提升用户体验，多进程可以让你在听网易云的时候打开微信和朋友聊天，多线程则是可以让你在听歌的时候还可以去看网易云的评论或者搜索其他歌曲。这就是多进程/多线程带给我们的好处。

由于多进程/多线程需要进行上下文切换才能实现，那上下文切换会带来什么问题呢？

- 增加了cpu的复杂度（需要保存和恢复上线程状态）。
- 处理器高速缓存被重新加载。
- 切换过于频繁可能会导致系统宕机。
- 某些场景下频繁的上下文切换还会导致效率变低



### 创建线程的方式有哪些？

**继承Thread**：继承 Thread 类并且重写 run() 方法，调用start() 方法启动线程。

```java
public class MyThread extends Thread {

    @Override
    public void run(){
        System.out.println("当前线程："+ Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        System.out.println("我是主线程："+ Thread.currentThread().getName());
        new MyThread().start();
    }
}
```

**实现Runnable接口**

定义一个实现 Runnable 接口的实现类，重写该接口的 run() 方法， 同时将该实现类的实例作为 Thread 实例化的参数，当调用 Thread 中 start() 方法时，启动线程。

```java
public class MyThread2 implements Runnable {
    @Override
    public void run() {
        System.out.println("当前线程："+ Thread.currentThread().getName());
    }


    public static void main(String[] args) {
        System.out.println("我是主线程："+ Thread.currentThread().getName());
        new Thread(new MyThread2()).start();
    }
}
```



**匿名内部类**

匿名内部类其实也是实现了 Runnable 接口，并重写了 run() 方法，因为这个类没有名字，直接作为参数传递给 Thead 类。

```java
public class MyThread3 {

    public static void main(String[] args) {
        System.out.println("我是主线程："+ Thread.currentThread().getName());
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("当前线程："+ Thread.currentThread().getName());
            }
        }).start();
    }
}
```



**实现Callable接口**

与使用Runnable相比， Callable功能更强大些。

- 支持**返回值**。
- 方法可以**抛出异常**。
- 支持**泛型**。
- 结合 FutureTask 可以获取到线程的**返回值**。
- FutureTask 同时实现了Runnable, Future接口。它既可以作为Runnable被线程执行，又可以作为Future得到Callable的返回值

```java
public class MyThread4 implements Callable<String> {

    @Override
    public String call() throws Exception {
        System.out.println("当前线程："+ Thread.currentThread().getName());

        return "hello";
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        System.out.println("我是主线程："+ Thread.currentThread().getName());
        Callable<String> tc = new MyThread4();
        FutureTask<String> task = new FutureTask<>(tc);
        new Thread(task).start();
        System.out.println(task.get());
    }

}
```



**线程池启动**

线程池创建线程是我们比较推荐使用的方式，理由如下：

- 重用存在的线程，减少对象创建、消亡的开销，性能佳。
- 可有效控制最大并发线程数，提高系统资源的使用率，同时避免过多资源竞争，避免堵塞。
- 提供定时执行、定期执行、单线程、并发数控制等功能。

```java
public class MyThread5 implements Runnable{

    @Override
    public void run() {
        System.out.println("当前线程："+ Thread.currentThread().getName());
    }

    public static void main(String[] args)  {
        System.out.println("我是主线程："+ Thread.currentThread().getName());
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.execute(new MyThread5());
    }
}
```



### 线程的生命周期？

了解线程的生命周期之前，我们需要知道线程的 6 种不同的状态，了解了它们，再来看线程的生命周期。

| 状态         | 描述                                                         |
| :----------- | :----------------------------------------------------------- |
| NEW          | **初始状态**，线程被创建，但是还没有执行start()方法。        |
| RUNNABLE     | **运行状态**（其实严格来分是两个状态：**就绪状态、运行中状态**），这是针对JVM层面的，对于CPU层面，调用了start()方法并不会马上被执行，而是进入了**就绪状态**这个与线程的优先级有关，而JVM将就绪状态和运行中状态统称为**运行中**。 |
| BLOCKED      | **阻塞状态**，当前线程阻塞于锁                               |
| WAITING      | **等待状态**，线程进入等待状态之后，需要等待其他线程完成之后做出通知，然后当前线程才有机会回到**就绪状态**，它自己无法结束等待状态 |
| TIME_WAITING | **超时等待状态**，该状态不同于 WAITING ，它支持在指定时间后主动返回 |
| TERMINATED   | **终止状态**，表示当前线程已经执行完毕，线程将会被销毁       |

如果想了解java程序在运行时候线程状态之间的转换，可以使用 jstack 工具查看。

接下来我们再来看看线程的生命周期是什么样的？

![线程生命周期](https://p6.toutiaoimg.com/origin/pgc-image/eb889f7d361e435faeae5c7239f2e6d1)

<center>
    <div style="color:orange; border-bottom: 1px solid #d9d9d9;
    display: inline-block;
    color: gray;
    padding: 2px;"><b>图 2-1</b></div>
    <br>
</center>



### Thread 类中的start() 和 run() 方法有什么区别？

start() 方法用于启动一个线程，让启动的线程处于就绪状态，等待 Cpu 的执行，而 run() 是线程真正执行的业务方法，run() 结束，线程也随之结束。

如果开启一个线程不是通过 start()  而是通过直接调用 run() ，那么只会在原来线程中执行，并不会开启一个新的线程，这点需要特别注意。

所以只有通过调用 start() 才能真正的创建一个新的线程。

### sleep()、wait()、yield()方法有什么区别？

- 等待池：假设一个线程A调用了某个对象的wait()方法，线程A就会释放该对象的锁后，进入到了该对象的等待池，等待池中的线程不会去竞争该对象的锁。
- 锁池：只有获取了对象的锁，线程才能执行对象的 synchronized 代码，对象的锁每次只有一个线程可以获得，其他线程只能在锁池中等待

**sleep()**：Thread中的静态方法，使当前线程进入指定时间的等待状态，该方法可以让让其他线程得到执行的机会，但是由于 sleep() 方法不会释放锁，只要是被synchronized 修饰的方法或者代码块，其他线程仍然不可以访问。

**wait()**：让线程进入等待状态，wait() 属于 Object 类中的方法，需要配合 notify() 及notifyAll() 两个方法一起使用，这三个方法用于协调多个线程对共享数据的存取，所以必须在synchronized语句块内使用。但和 sleep() 不同的是 wait() 会释放锁标志，让其他线程可以访问被 synchronized  修饰的发放或者代码块，当调用 wait() 方法之后，当前线程将会暂停执行，并将当前线程放入对象等待池中，直到有线程调用了 notify() 及notifyAll()  其中的一个或者等待时长超过了设置超时时间，被暂停的线程将会被恢复到锁池。需要注意的是 notify() 只会随机等待池中的一个线程进入锁池，而 notifyAll() 唤醒对象的等待池中的所有线程，进入锁池。

**yield()**：让当前线程从运行状态进入就绪状态，从而可以让出cpu的执行权，应为就绪的线程能否被执行取决于线程的优先级（默认情况下），优先级高的线程被执行的概率较高，这并不能说明优先级低的线程就一定会比优先级高的线程后执行。执行 yield() 之后的线程也有可能会立马获取到cpu的执行权限，因为它处于就绪状态，所以说，虽然 yield() 让出了 cpu 的执行权，但是有可能因为当前现成的优先级很高，在竞争中又拿到了 cpu 的执行权，不要觉得执行了  yield() 的线程一定会让其他线程享有cpu执行权，他们全靠竞争。 yield() 只是让当前线程回到了和其他线程一样的起点。



### 终止线程的方法有哪些？

**正常结束**：执行完 run() 方法主动结束。

**stop()方法**

由于 stop() 被标记为 @Deprecated ，说明当前方法已经不推荐使用，为什么不推荐使用呢？那是因为  stop()  属于暴力停止，他会强制当前线程结束自己的生命周期，这样会导致数据的不完整，也就是说会破坏程序的原子性，请看下面例子

```java
public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(() ->{
            System.out.println("当前线程：+"+Thread.currentThread().getName() +" 被执行了");
            try {
                sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("当前线程：+"+Thread.currentThread().getName() +" 休眠结束了");
        });
        thread.start();
        Thread.sleep(3000);
        thread.stop();
        System.out.println("main 线程结束了");
    }
```

在main 函数中创建了一个子线程 thread，让子线程休眠10秒，然后在 main 函数休眠 3 秒之后调用子线程的 stop() 方法，这个时候 thread 线程会强制结束，控制台就不会输出 “休眠结束了”这句话，这也就会导致程序的不完整，可以将他理解为数据库中的事务，在我们正常的开发中，可能会造成数据的错误，这是一个很严重的问题，所以**一定不要使用 stop() 停止线程**。

**退出标志法**：

使用退出标识，使得线程正常退出，即当run方法完成后进程终止。我们在线程中创建一个线程安全的标志字段，线程中通过判断此字段来确定是否需要退出线程。

```java
public class MarkStopThreadTest implements Runnable {

    private static  volatile boolean STOP_FLAG;

    @Override
    public void run() {
        System.out.println("线程被执行了");
        while (!STOP_FLAG){
            //线程执行的时候
        }

    }

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new MarkStopThreadTest());
        thread.start();
        //这里休眠500毫秒为了让线程被cpu值执行
        Thread.sleep(500);
        System.out.println("当前线程状态："+thread.getState());
        System.out.println("修改标志状态-----");
        STOP_FLAG = true;
        //这里休眠500毫秒为了让线程成功结束当前任务
        Thread.sleep(500);
        System.out.println("当前线程状态："+thread.getState());
    }
}
```

STOP_FLAG：标志位，false 表示线程需要继续执行，true 表示当前线程需要结束了。



**interrupt()方法**：中断线程，这并不是代表当前线程会立马结束，这里只是修改了线程的 status ，线程什么时候结束，还是需要结束线程自己说了算。

当线程阻塞在调用方法时（wait()、join()、sleep()等），调用 interrupt()时，线程的终端状态将会被清除，会收到一个 InterruptedException 异常。

当线程阻塞在IO操作时， 通道将会被关闭，线程的中断状态将被设置，同时线程会收到一个 ClosedByInterruptException 异常。

当线程阻塞在NIO java.nio.channels.Selector（选择器）时，线程的中断状态将被设置，同时线程将会立即从选择器操作中返回，可能会带有一个非零的值，就像选择器的 java.nio.channels 调用了java.nio.channels.Selector的wakeup方法。

下面请看一个终止一个线程正在阻塞在sleep()方法的demo

```java
public class StopThreadByInterrupt {

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(() -> {
            System.out.println("当前线程被执行了----------");
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println("休眠被终止了");
            }
            System.out.println("休眠结束，继续往下执行需要执行的任务");
        });
        thread.start();
        //这里休眠500毫秒为了让线程被cpu值执行
        Thread.sleep(500);
        System.out.println("当前线程状态："+thread.getState());
        System.out.println("调用Interrupt方法结束线程-----");
        thread.interrupt();
        //这里休眠500毫秒为了让线程成功结束当前任务
        Thread.sleep(500);
        System.out.println("当前线程状态："+thread.getState());
    }
}
```

当我们调用 interrupt() 的时候，sleep()会立马中断，然后继续往下执行，并没有直接将线程结束，所以一般结束线程的时候推荐使用 interrupt()，一面造成不可弥补的损失。



### synchronized与Lock有什么异同?

**归属**：synchronized 属于 jdk 自带，而 Lock 属于 SDK 并发包中的内容。

**用法**：synchronized 可以在方法上、代码块中使用，而 Lock 必须要指定 锁定的起始和结束位置。

**性能上**：资源竞争较小时，synchronized 优于 Lock；相反资源竞争很大的时候，synchronized  的性能会下降的很快，Lock完胜   。

**功能性**：synchronized一个非公平锁， 可重入，但是不可中断的锁，而 Lock 支持可重入、可中断、可公平的锁。

| 功能         | synchronized               | Lock                                     |
| ------------ | -------------------------- | ---------------------------------------- |
| 中断性       | ✖                          | ✔                                        |
| 可重入       | ✔                          | ✔                                        |
| 可重试       | ✖                          | ✔                                        |
| 自动释放锁   | ✔                          | ✖                                        |
| 锁类型       | 可重入、不可中断、非公平锁 | 可重入、可中断、非公平锁（默认）、公平锁 |
| 锁定多个条件 | ✖                          | ✔                                        |



### 什么是守护线程？
JVM 一共提供了两种线程类型，一种是**用户线程**,另外一种就是**守护线程**。

- **用户线程**：高优先级线程。程序中JVM 会在终止之前等待任何用户线程完成其任务。
- 守护线程：低优先级线程。其唯一作用是为用户线程提供服务。

守护线程的作用是为用户线程提供服务，并且在用户线程运行的才会存在，java程序启动时至少会存在一个 main线程（用户线程），此时守护线程
也会被创建，main线程又可以创建很多的用户线程，只要有用户线程还在运行，那么 JVM 就不会退出，当所有用户线程都已经执行完毕，包括 
main 线程，JVM就会立刻退出，守护线程也就结束了它们的生命周期。

正常来说守护线程是不会组织 JVM 的退出，但是如果因为设计的草率，在守护线程中调用 Thread.join()，这个时候守护线程就会阻止 JVM退出。

守护线程在 JVM 运行中主要在处理垃圾回收任务、释放资源、后台支持、过期策略等等，我们知道。在我们开发中，我们并没有去写垃圾回收相关
的代码，就是因为这是由守护线程替我们完成的。

如何创建守护线程？
```java
public static void main(String[] args) throws InterruptedException {
       Thread thread = new Thread(() ->{
           System.out.println("我是守护线程-----------"+Thread.currentThread().getName());
           System.out.println("是否为守护线程："+ Thread.currentThread().isDaemon());
       });
        //设置当前线程为守护线程
       thread.setDaemon(true);
       thread.start();
        Thread.sleep(1000);
        System.out.println("结束");
}
```



### Java内存模型是什么？

并发编程中，同步和通信这两个问题是需要处理的关键问题。

**通信**：在共享内存模型中，线程之间共享程序的公共状态，通过写-读内存中的公共状态进行隐式通信。在消息传递的并发模型中，线程之间没有公共状态，线程之间必须通过发送消息来显式的通信。

**同步**：线程同步指程序中用于控制不同线程之间操作发生相对顺序的机制，程序必须需要显式的指定某个方法或者某个代码块在线程之间互斥执行。在消息传递的并发模型中，由于消息的发送必须在消息接收之前，因此同步时隐式进行的。

**什么是java内存模型？**

java线程之间的通信由java内存模型（JMM）控制，JMM决定一个线程对共享变量的写入如何使对另一个线程可见。从抽象的角度来看，JMM 定义了线程和主内存之间的抽象关系：线程之间的共享变量存储在主内存中，每个线程都有一个私有的本内存。本地内存中存储了该线程以读/写共享变量的副本。**本地内存是 JMM的一个抽象概念，并不真实存在**。它涵盖了缓存、写缓冲区、寄存器以及其他的硬件和编译器优化。

![JMM](https://p3.toutiaoimg.com/large/tos-cn-i-qvj2lq49k0/8d3268ee354543fc8a268d9acfdbdcdd)

<center>
    <div style="color:orange; border-bottom: 1px solid #d9d9d9;
    display: inline-block;
    color: gray;
    padding: 2px;"><b>图 2-2</b></div>
    <br>
</center>

从图形中我们可以看到，如果线程A与线程B之间需要通信，那么必须要经过以下两个步骤。

**1：线程A把本地内存A中更新过的共享变量刷新到主内存中。**

**2：线程B到主内存中读取线程A之前已经修改过的共享变量。**

本地内存A、B都只会存储主内存中共享变量的副本，然后线程A、B各自对副本中的变量进行操作，最后将副本中的数据刷新到主内存中，这就是完整的一条流程。

在多线程横行的时代，这两个步骤按照目前这样走法，肯定是存在线程安全问题的，所以这里就要涉及到并发编程BUG来源：原子性、可见性、有序性（后面会讲到），这也是java内存模型中最重要的内容之一了。



### 什么是原子性、可见性和有序性？

**原子性**：一个或多个操作要么全部执行成功，要么全部不执行，我们称这种为原子性，把它当作是一个最小的执行单位，这样就不会发生线程安全问题。

线程切换导致的原子性问题。

早期时代，发明了多进程，让用户可以一边听歌一边敲代码，操作系统选择一个进程执行一段时间之后（假设20毫秒），操作系统就会选择另外一个进行来进行操作，我们成这种行为为 “任务切换”，20毫秒称为 “时间片”。

由于进程之间变量不共享，所以也就不会存在并发的问题，但是java的执行单位是线程，和进程一样，也会伴随着任务切换（我们称它为上下文切换），但是由于线程属于同一个进程中，而进程中变量共享，所以这个时候，任务切换就成了并发编程诡异BUG的源头之一了。

如：i+=1 这一条简单的 +1 操作，对于 cpu而言，需要3条执行才能完成。

> 指令1：把共享变量 i 加载到 cpu 的寄存器。
>
> 指令2：在寄存器中执行 +1操作。
>
> 指令3：将结果写主内存（由于缓存机制的存在，此时写回的可能是 cpu缓存而不是主内存）。

假设 i 初始值为 0，线程A、B同时执行 +1 操作，由于任务切换的存在，线程A 执行了指令1（获取到 i = 0）、指令2（i = 1，还没写回到内存中），此时，cpu将执行权切换到了线程B，执行了指令1（获取到 i = 0，因为线程A并没有将 i=1写回主内存）、指令2（i = 1），指令3，最后 cpu 切回线程 A ，执行指令3，这样，bug 就出现了， i的最终结果是1，而不是2.

如何保证程序的原子性？

- **1.锁：顾名思义，将需要原子性的操作通过加锁的方式来保证原子性（比如java中的 synchronized）。**
- **2.原子类工具：java 并发工具包中提供了很多原子类的工具类，实现原理是利用 CAS 保证共享变量的安全性（比如：AtomicInteger、AtomicILong、AtomicBoolen）。**



**可见性**：当一个线程修改了共享变量时，另一个线程可以读取到这个修改后的值。这在单核时代中没有任何问题，随着技术的提升，双核、四核、八核 CPU 的出现，让并发编程也开始出现了大量的问题，比如，共享变量的可见性问题。

**缓存导致共享变量的可见性问题**：在java内存模型中我们可以知道每个线程都有自己的一个工作内存，他并不是真实存在的，你可以理解为 CPU的高级缓存，而多核时代，就代表着有多个高级缓存，这样其中一核修修改了数据但是还没刷新到主内存中时，其他线程从主内存获取的共享变量就是旧的，而拿不到修改后的数据，这就是可见性问题。

**如何保证线程的可见性**：

- **共享变量增加 volatile关键字。**
- **程序加锁-synchronized。**
- **将共享变量设置为 final。**



**有序性**

高级编程语言中，编译器在编译阶段，往往会对我们写的代码进行优化，其中就包括**指令重排**，**指令重排是指编译器核处理器为了优化程序性能而对执行序列进行重新排序的一种手段**。

但是指令重排不能影响程序的最终结果，在单核时代，确实没有问题，但是多核时代，诡异 BUG就来了。

```java
public class RestTemplateConfig {
    static RestTemplate restTemplate;
    public RestTemplate getRestTemplate(){
        if(null == restTemplate){
            synchronized (RestTemplateConfig.class){
                if(null == restTemplate()){
                    return  new RestTemplate();
                }
            }
        }
        return  restTemplate;
    }
}
```

这是一个简单单例的获取方式，双重校验的方式，这么一看确实没有什么问题，那究竟会发生什么呢？

在获取 getRestTemplate() 的方法中，我们首先判断 restTemplate是否为空，空着锁定 RestTemplateConfig.class，然后创建一个新的 RestTemplate 实例，看上去无懈可击，但是聪明的人通过上面的标题可能已经知道问题出现在哪里了。

假设 两个线程 A、B，同时调用 getRestTemplate() 方法，判断 null == restTemplate 都为空，同时加锁，A成功、B等待，此时线程 A 创建了一个新的 RestTemplate 实例并且返回，然后释放锁，线程 B 获取锁，判断实例已经存在，释放锁直接返回实例。

创建 RestTemplate 实例，我们期望的步骤是这样的：

- 1.分配一块内存 M。
- 2.在内存 M 上 初始化 RestTemplate 对象。
- 3.然后将 M 的地址赋值给 restTemplate 变量。

但是由于指令重排的存在，创建 RestTemplate 实例，执行步骤可能是这样的：

- 1.分配一块内存 M
- 2.将 M 的地址赋值给 restTemplate 变量；
- 3.最后在内存 M 上初始化 RestTemplate 对象。

这也就会导致线程 A 在初始化 RestTemplate 实例 之后，先赋值了restTemplate 变量，此时发生了线程切换，来到了线程 C ，线程 C 在第一个判断中就发现变量 

restTemplate 有值，不需要去进行加锁创建对象的操作，直接返回，这样就会导致栈中的变量 restTemplate 有值，但是堆上的对象还没有被创建，从而抛出空指针异常。

不过这个问题在 JDK 1.6 就被修复了，这里只是举了一个例子，用来说明指令重排也是造成多线程诡异 BUG 之一，不可轻视。

解决这个问题其实也挺简单，只要禁止指令重排即可。



### 什么是 happens-before规则？

在JMM（JSR-33）中，如果一个操作执行的结果需要对另一个操作可见，那么这两个操作之间必须要存在 happens-before 关系。这里提到的两个操作既可以是在一个线程之内，也可以是在不同线程之间， happens-before 就是为了解决线程可见问题。

**happens-before规则如下：**

- **顺序规则：一个线程中的每个操作， happens-before 于该线程中的任意后续操作。**
- **监视器锁规则：对一个锁得解锁， happens-before 于随后对这个锁的加锁。**
- **volatile变量规则：对一个 volatile 域的写，happens-before 域任意后续对这个 volatile 域的读。**
- **传递性：如果 A happens-before B ，且 B happens-before C ，那么 A happens-before C。**

这 4 调规则 你可以记不住，但是这句话一定要理解：**happens-before 保证了前面一个操作的结果对后续操作是可见的**。

![JMM于happens-before](https://gitee.com/yq0601/interview-collection/raw/master/images/JMM%E4%BA%8Ehappens-before-1638196736772.jpg)

<center>
    <div style="color:orange; border-bottom: 1px solid #d9d9d9;
    display: inline-block;
    color: gray;
    padding: 2px;"><b>图 2-3</b></div>
    <br>
</center>

如图 2-3 所示，一个happens-before 规则对应于一个或多个编译器核处理器重排序规则。对于java程序员来说，happens-before 规则很简单，他避免 java 程序员为了理解 JMM 提供的内存可见性保证而去学习复杂的重排序规则以及这些规则的具体实现方法。



### 简单介绍一下volatile 变量是什么？

**volatile特性**

一个 volatile变量的单个读/写操作与一个普通变量的读/写操作都是用同一个同步锁，执行的效果是一样的。

锁的 happens-before 规则保证锁的释放和获取锁的两个线程之间内存可见，这意味着对一个volatile变量的读，总是能看到任意线程对这个 volatile 变量的最后的写入。

**只要是变量被 volatile 修饰，对该变量的读/写就具有原子性。如果是多个 volatile 操作或类似 volatile++ 这种符合操作，无法保证原子性。**

总体来说，volatile 具备以下两个特性：

- **可见性：对一个 volatile 变量的读，总是能看到任意线程对这个 volatile 变量最后的写入。**
- **原子性：对任意单个 volatile 变量的读/写具有原子性，但类似 volatile++ 这种符合操作不具备原子操作。**

**volatile 和happens-before的关系**

自从 JSR-133 开始，volatile 变量的写 - 读可以实现线程之间的通信。

volatile的写 - 读 于锁的释放 - 获取有着相同的内存效果：volatile 写和锁的释放具有相同的内存语义；volatile 读于锁的获取有相同的内存语义。

这段可以结合happens-before规则一起看，效果更佳。

总体来说，**volatile 可以保证变量的内存可见性，禁止指令重排**。

### 如何在两个线程间共享数据？

如果执行的是相同代码，那么可以通过使用同一个Runnable对象，对象中的属性共享。

如果执行的是不同代码，将共享数据封装到一个独立的对象中，然后将这个对象传递给不同的线程，所有对数据的操作都在数据封装的对象中，这样也能实现线程之间的数据共享。

将Runnable对象作为一个类的内部类，共享数据作为这个类的成员变量，每个线程对共享数据的操作方法也封装在外部类，以便实现对数据的各个操作的同步和互斥，作为内部类的各个Runnable对象调用外部类的这些方法。



### Java中notify 和 notifyAll有什么区别？

这里还是需要结合前面讲到的两个概念：**锁池**与**等待池**。

如果线程调用了对象的 wait()方法，那么线程便会处于该对象的**等待池**中，等待池中的线程**不会去竞争该对象的锁**。

但是当线程调用了对象的 notify() 或者notifyAll() 之后，被唤醒的线程将会进入该对象的**锁池**，锁池中的线程会去竞争该对象的锁。，也就是说，调用了 notify() 后只有一个线程会由等待池进入锁池，而notifyAll会将该对象等待池内的所有线程移动到锁池中，等待锁竞争。

一般 notify() 与 notifyAll()  都是和 wait() 一起使用，我们称之为：**等待/通知机制**。

一个线程 A 调用了对象 O 的 wait() 方法进入等待状态（进入等待池），而另一个线程 B 调用了对象 O 的 notify() 或 notifyAll() 方法，线程 A 收到通知后从对象 O 的 wait() 方法中返回（进入锁池，如果有多个线程，这里会涉及到锁竞争），进而继续执行后续操作。上述两个线程通过对象 O 来完成交互，而对象上的 wait()、notify()、notifyAll() 的关系就如同开关信号一样，用来完成等待方会和通知方之间的交互工作。



### 为什么wait, notify 和 notifyAll这些方法不在thread类里面？

由于wait，notify() 和notifyAll()都是锁级别的操作，所以把他们定义在Object类中，因为锁属于对象。



### 什么是线程池？ 为什么要使用它？

每个线程都有自己的生命周期，并且线程属于 cpu 级别的指令，比较重，创建和销毁比较耗时，如果我们在程序启动的时候就创建好一些线程，我们需要使用的时候直接使用即可，省去了线程的创建节约了时间，也提升了效率，spring 就提供了这样的功能，没错，就是线程池。

**线程池的核心参数**

**corePoolSize**：核心线程数，核心线程会一直存活，即使没有任务需要执行，当线程数小于核心线程数时，即使有线程空闲，线程池也会优先创建新线程处理，设置allowCoreThreadTimeout=true（默认false）时，核心线程会超时关闭。

**queueCapacity**：任务队列容量（阻塞队列），当核心线程数达到最大时，新任务会放在队列中排队等待执行（队列先进先出）。

**maxPoolSize**：最大线程数：当线程数>=corePoolSize，且任务队列已满时。线程池会创建新线程来处理任务；当线程数=maxPoolSize，且任务队列已满时，线程池会拒绝处理任务而抛出异常。

**keepAliveTime**：线程空闲时间，当线程空闲时间达到keepAliveTime时，线程会退出，直到线程数量=corePoolSize，如果allowCoreThreadTimeout=true，则会直到线程数量=0。

**allowCoreThreadTimeout**：允许核心线程超时。

**rejectedExecutionHandle**r：拒绝策略，当线程池的任务缓存队列已满并且线程池中的线程数目达到maximumPoolSize时，如果还有任务到来就会采取任务拒绝策略，主要有四种拒绝策略

- ThreadPoolExecutor.AbortPolicy:丢弃任务并抛出RejectedExecutionException异常。

* ThreadPoolExecutor.DiscardPolicy：务丢弃任，但是不抛出异常。
* ThreadPoolExecutor.DiscardOldestPolicy：丢弃队列最前面的任务，然后重新提交被拒绝的任务
* ThreadPoolExecutor.CallerRunsPolicy：由调用线程（提交任务的线程）处理该任务



**java常见的线程池**

newCachedThreadPool：创建一个可缓存线程池，如果线程池长度超过处理需要，可灵活回收空闲线程，若无可回收，则新建线程。

newFixedThreadPool：创建一个指定工作线程数量的线程池，每当提交一个任务就创建一个工作线程，如果工作线程数量达到线程池初始的最大数，则将提交的任务存入到池队列中。该线程在提升效率和节省线程创建时间有很大优势，但它同时也存在不足点，当线程池空闲时，即使线程池中没有任务正在执行，该线程池也不会释放工作线程，会消耗额外的资源。

newSingleThreadExecutor：创建一个单线程化的Executor，顾名思义，该线程池只会创建一个工作线程来执行任务，保证所有的任务都按照指定顺序（FIFO, LIFO, 优先级）执行。如果在执行过程中，线程发生异常终止，那会将会有一个新的线程代替它，继续保证顺序执行，这也是单个线程的优势（保证线程执行的顺序行）。

newScheduleThreadPool：创建一个支持定时以及周期性执行任务的线程池，支持定时及周期性任务执行。



**线程池应用场景**

- 日志记录。
- 发送通知类业务。
- 持久化。
- 批量数据处理。
- 肖峰（mq会更合适）



线程池注意事项

- 多线程容易出现死锁（这并不是线程池才会出现，只要有多线程就可能会存在）。
- 任务队列尽量选择有界队列，无界队列在极端条件下容易造成OOM。
- bug诡异，追踪复杂编写多线程程序时一定要考虑边界值问题。
- 异步转同步问题。



### 什么是cas？
**概念**
比较并交换(compare and swap, CAS)，是原子操作的一种，可用于在多线程编程中实现不被打断的数据交换操作，从而避免多线程同时改写
某一数据时由于执行顺序不确定性以及中断的不可预知性产生的数据不一致问题。 该操作通过将内存中的值与指定数据进行比较，当数值一样
时将内存中的数据替换为新的值。

CAS 是原子操作，可以保证线程的安全，但是无法保证线程的同步，它是 CPU 的一条指令，非阻塞、轻量级的乐观锁。



**原理**
CAS的核心就是比较和交换两个动作， 内存 中存放着一个变量 V，我们在对这个变量进行修改时，会有两个操作，一个是修改前的旧值（期望值：期望和内存中的值一致），另外一个是新值（修改后的值），进行修改的时候，cas 会 先判断旧值（期望值）和内存中目前存在的值是否相同，如果相同，则将内存中的值修改为新值，如果不相等，则说明该变量已经被其他线程修改，当前修改线程需要自旋（也就是循环-死循环），自旋的时候会将内存中的值重新取出来，直到期望值和内存中相同，再执行交换动作，也就是将新值写道内存中。



**java cas 实现原理源码**

```java
public final int getAndAddInt(Object var1, long var2, int var4) {
        int var5;
        do {
            var5 = this.getIntVolatile(var1, var2);
        } while(!this.compareAndSwapInt(var1, var2, var5, var5 + var4));

        return var5;
    }
```

cas无锁只是相对于编程语言的，对 CPU 而言，cas 同样存在锁机制，不然，两步操作有先后顺序，多线程的时候肯定存在并发问题，java cas原子性是由 Unsafe 保证的，而他是C++ 级别的。



**cas 缺点**

一、自旋浪费 cpu 资源。如果一个共享变量可能会被经常修改，比如双十一的商品库存，可能一秒钟都会改变几万次甚至几十万次，这个时候使用 cas 就会适得其反。

二、ABA问题。将设内存中的变量初始值 V = 10，有三个线程同时对它进行修改操作，进行第一步的时候三个线程获取到的值都是10，线程一首先进行对比、交换成功，将 V 改成了 20，此时线程 2 在进行比较的时候发现对不上了，所以获取到了新值 V = 20 ，在将值修改成了 10，这是切换到线程 3，他的期望值也是10 ，与内存中的值进行对比，发现相同，判定符合期望，然后将新值 15 替换到内存中，这就是 cas 可能会出现的 ABA 问题。这个问题说大不大，说小也不小，有些业务可能会造成不可挽回的损失，所以在使用 cas 的时候一定要注意。

**如何解决ABA问题**

解决ABA问题其实很简单，多引入一个参数即可，版本号，每次修改版本号+1，判断值是否相同的同时还需要判断版本号是否对的上，这样就能很好的解决 cas 中 ABA的问题，java 提供了需要解决 ABA 问题的 sdk 并发包，例如：**AtomicStampedReference、AtomicMarkableReference** 等等。




### 谈谈你对AQS的理解

AbstractQueuedSynchronizer：提供一个框架来实现依赖于先进先出 (FIFO) 等待队列的阻塞锁和相关的同步器（信号量、事件等）。此类旨在为大多数依赖单个原子`int`值表示状态的同步器提供有用的基础。子类必须定义更改此状态的受保护方法，并定义该状态在获取或释放此对象方面的含义。鉴于这些，此类中的其他方法执行所有排队和阻塞机制。子类可以维护其他状态字段，但只有`int` 使用方法操作的原子更新值**getState()、setState(int)、compareAndSetState(int, int)**在同步方面被跟踪。

子类应定义为非公共内部帮助类，用于实现其封闭类的同步属性。类 AbstractQueuedSynchronizer 不实现任何同步接口。相反，它定义了一些方法，例如 acquireInterruptibly(int) 可以由具体的锁和相关的同步器适当调用以实现它们的公共方法。

此类支持默认独占 模式和共享模式中的一种或两种。以独占模式获取时，其他线程尝试获取时不会成功。多个线程的共享模式获取可能（但不一定）成功。此类不“理解”这些差异，除非在机械意义上，当共享模式获取成功时，下一个等待线程（如果存在）也必须确定它是否也可以获取。不同模式下等待的线程共享同一个FIFO队列。通常，实现子类只支持其中一种模式，但两者都可以发挥作用，例如在 ReadWriteLock . 只支持独占或只支持共享模式的子类不需要定义支持未使用模式的方法。

此类定义了一个嵌套 AbstractQueuedSynchronizer.ConditionObject 类，可以由支持独占模式的子类用作Condition实现，该方法isHeldExclusively() 报告是否针对当前线程独占保持同步，release(int)  使用当前值调用的方法getState()完全释放此对象，并且acquire(int)，给定此保存状态值，最终将此对象恢复到其先前获得的状态。没有 AbstractQueuedSynchronizer 其他方法会创建这样的条件，因此如果无法满足此约束，请不要使用它。的行为AbstractQueuedSynchronizer.ConditionObject 当然取决于其同步器实现的语义。

此类为内部队列提供检查、检测和监视方法，以及用于条件对象的类似方法。这些可以根据需要使用AbstractQueuedSynchronizer 它们的同步机制导出到类中。

此类的序列化仅存储底层原子整数维护状态，因此反序列化对象具有空线程队列。需要可序列化的典型子类将定义一个readObject 方法，该方法在反序列化时将其恢复到已知的初始状态。

### 什么是ThreadLocal? 

ThreadLocal 是Thread 的局部变量，用于编多线程程序，对解决多线程程序的并发问题有一定的启示作用。

概念很抽象，其实只要记住一句话即可，为每一个线程创建一个变量，线程之间变量不共享，变量的生命周期与线程的生命周期一致，可以参考方法中的局部变量，没有竞争，所以不会存在线程安全问题，所以在并发编程中，ThreadLocal  被广泛的使用。

**我们如何使用 ThreadLocal 呢？**

ThreadLocal 构造允许我们存储**只能**由**特定线程**访问的数据。假设我现在需要在某个线程中使用一个整形的变量 threadLocal，我们只需要这样设计即可：

```java
ThreadLocal<Integer> threadLocal = new ThreadLocal<>();
```

num 已经初始化完成，那我们应该如何使用呢？方法很简单， ThreadLocal 为我们提供了两个操作方法：get()、set() 方法，ThreadLocal 会以线程为单位操作num变量。

```java
threadLocal.set(1); 
Integer result = threadLocal.get();
```

如果我们希望在初始化的时候就能给 threadLocal 赋上初始值，那么我们只需要借助 ThreadLocal  的静态方法：withInitial() 即可实现（jdk 8 以上）。

```java
ThreadLocal<Integer> threadLocal  = ThreadLocal.withInitial(() -> 100);
```

上面代码表示初始化 threadLocal 并且将 threadLocal 赋值为 100。

**源码理解**

```java
public void set(T value) {
        Thread t = Thread.currentThread();
        ThreadLocalMap map = getMap(t);
        if (map != null)
            map.set(this, value);
        else
            createMap(t, value);
    }

```

第一步获取当前线程，然后使用当前线程作为参数查询 ThreadLocalMap 如果存在，执行赋值操作，如果不存在，实行初始化创建操作。

```java
 void createMap(Thread t, T firstValue) {
        t.threadLocals = new ThreadLocalMap(this, firstValue);
    }

ThreadLocalMap(ThreadLocal<?> firstKey, Object firstValue) {
            table = new Entry[INITIAL_CAPACITY];
            int i = firstKey.threadLocalHashCode & (INITIAL_CAPACITY - 1);
            table[i] = new Entry(firstKey, firstValue);
            size = 1;
            setThreshold(INITIAL_CAPACITY);
        }

static class Entry extends WeakReference<ThreadLocal<?>> {  //WeakReference 弱引用
            /** The value associated with this ThreadLocal. */
            Object value;

            Entry(ThreadLocal<?> k, Object v) {
                super(k);
                value = v;
            }
        }

```

createMap(Thread t, T firstValue) 初始化了一个 ThreadLocalMap，key 为 ThreadLocal value 为线程的变量值。但是 ThreadLocalMap 并不是归 ThreadLocal 持有，而是存在于 Thread 中。

很明显 table 才是 ThreadLocal 的核心，存放着 ThreadLocal  的变量，通过 Entry 的源码我们可以知道，Entry 对 ThreadLocal  对象的引用为弱引用，正常来说，ThreadLocal  生命周期结束，Entry 也会被回收，但是事实上却不是这样的，可以看下面的内存泄漏问题。

**为什么不将 ThreadLocalMap 归为 ThreadLocal  持有？**

1. ThreadLocal  仅仅只是一个工具类，内部并不持有任何和线程相关的数据，所有和线程相关的数据都会存储在 Thread 中。
2. 不容易产生内存泄漏。如果 ThreadLocal  持有 ThreadLocalMap，ThreadLocalMap 会持有 Thread 对象的引用，所以只要 ThreadLocal  对象存在，那么 ThreadLocalMap对象中的 Thread 对象将永远无法回收，而 ThreadLocal  的生命周期明显长于 Thread，所以很容易造成内存泄漏。

**ThreadLocal 与内存泄露**

真实开发中，一般都是使用线程池，这个时候，线程使用完成之后不会马上回收，尤其是核心线程，这就会导致 Thread 对象中的 ThreadLocalMap 也不会被回收，即使 ThreadLocalMap 中的 Entry 对 ThreadLocal 是弱引用（WeakReference），ThreadLocal  生命周期结束就会被回收，但是 Entry 中的 Value 却是被 Entry 强引用的，就算 value 的生命周期结束，也无法回收。

**如何解决内存泄露问题**

手动释放，添加try、finally 代码块，在 finally 中调用 ThreadLocal  对象中的 remove() 进行手动释放。



### 死锁是什么？如何避免死锁？ 

死锁（英语：deadlock），又译为死结，计算机科学名词。当两个以上的运算单元，双方都在等待对方停止执行，以获取系统资源，但是没有一方提前退出时，就称为死锁。在多任务操作系统中，操作系统为了协调不同线程，能否获取系统资源时，为了让系统正常运作，必须要解决这个问题。另一种相似的情况称为“活锁”。

举例：线程 1  操作为：用户 A 给用户 B 转账 100 元，线程 2 操作为 用户 B 给用户 A 转账 50 元，为了线程安全问题，线程 1 需要先锁住用户 A 的账户 然后锁住用户 B 的 操作，执行转账功能（A用户减 100 ，用户 B 加 100），不巧的这个时候 线程 2 先锁住了 B 的账户，同时需要获取用户 A 的锁，这个时候就发生了这样的一件事情，线程 1 等待 线程 2 释放 用户 B 的账户锁，线程 2 等待线程 1 释放 用户 A 的锁，如果没有人为干预，那么他们将会等到海枯石烂，抱憾终生，这就是死锁。

虽然死锁的后果很严重，但是产生死锁的条件很苛刻，我们只要注意好以下几点，死锁并不可怕。

**死锁产生的条件**

1. **互斥条件**：指进程(线程)对所分配到的资源进行排它性使用，即在一段时间内某资源只由一个进程(线程)占用。如果此时还有其它进程(线程)请求资源，则请求者只能等待，直至占有资源的进程用毕释放。
2. **请求和保持条件：**指进程(线程)已经保持至少一个资源，但又提出了新的资源请求，而该资源已被其它进程(线程)占有，此时请求进程(线程)阻塞，但又对自己已获得的其它资源保持不放。
3. **不剥夺条件：**指进程(线程)已获得的资源，在未使用完之前，不能被剥夺，只能在使用完时由自己释放。
4. **环路等待条件：**指在发生死锁时，必然存在一个进程(线程)——资源的环形链，即进程集合{P0，P1，P2，···，Pn}中的P0正在等待一个P1占用的资源；P1正在等待P2占用的资源，……，Pn正在等待已被P0占用的资源。

死锁必须满足上述四个条件才会触发，如果我们想要预防死锁，只要破坏以上 4 个条件中的人一个即可。

系统也可以尝试回避死锁。因为在理论上，死锁总是可能产生的，所以操作系统尝试监视所有进程，使其没有死锁。

**死锁发生后如何消除死锁**

重启进程，这个方法简单粗暴，但是重启带来的停服问题可能会带来较大的损失（可以采用多节点解决重启停服的问题）。

将进程回滚到先前的某个状态，如果一个进程被多次回滚，迟迟不能占用必需的系统资源，可能会导致资源匮乏。

**什么是活锁？**

**活锁**（livelock），与死锁相似，死锁是行程都在等待对方先释放资源；活锁则是行程彼此释放资源又同时占用对方释放的资源。当此情况持续发生时，尽管资源的状态不断改变，但每个行程都无法获取所需资源，使得事情没有任何进展。

举例：

你骑车上班，快迟到了，决定超小道，经过一个胡同的时候，出现了一个精神小伙，同样骑着小黄车，你们相遇了，但是你们的脾气都很爆，谁也不想让谁，都想让对方让自己先通过，然后你上班就迟到了，这就是死锁。

第二天，你又经过了这个胡同，又遇见了他，但是你吸取到昨天的教训，你想让对方先过，可能是你们昨天都给彼此留下了深刻印象，他也主动的给你让道，你们同时让道，但可惜的是你们礼让的方向都是同一侧，然后你们继续调整礼让位置，不巧的是你们又碰到了同一侧，然后你们就会一直这样让下去，所以你上班又迟到了，这个就是活锁。



### 请介绍一下synchronized锁升级

要搞明白什么是 synchronized 锁升级，就得先搞清楚什么是 synchronized？前面有介绍，可以往前面翻。

当我们聊到 synchronized 的时候，很多人第一反应就是他是一个重量级锁，没错，这也是为什么 JDK 团队在 1.6 对 synchronized 做了大量的优化，其中就包括 synchronized 的锁升级。

**对象头**

对象头，也就是我们经常说到的 **mark Word**，而我们的 synchronized 使用的锁就是存放 java 对象头中。**如果对象是数组类型，则虚拟机用3个字宽存储对象头，如果是非数组类型，则用 2 字宽存储对象头。在 32 位虚拟机中， 1 字宽等于 4 字节（32 bit）**。

java对象头的长度

|   长度    |          内容          |               说明               |
| :-------: | :--------------------: | :------------------------------: |
| 32/64 bit |       Mark Word        |  存储对象的 hashCode 或锁信息等  |
| 32/64 bit | Class Metadata Address |      存储对象类型数据的指针      |
| 32/32 bit |      Array length      | 数组的长度（如果对象类型是数组） |



java 对象头里的 Mark Word 默认存储对象的HashCode、分代年龄和锁标记位。32位 JVM 的Mark Word 的默认存储结构如下所示

|  锁状态  |     25 bit      |    4 bit     | 1 bit 是否是偏向锁 | 2 bit 锁标志位 |
| :------: | :-------------: | :----------: | :----------------: | :------------: |
| 无锁状态 | 对象的 hashCode | 对象分代年龄 |         0          |       01       |

在运行中， Mark Word 存储的数据会随着锁标志位的变化而发生变化，具体如下图所示。

![image-20220220143552751](https://gitee.com/yq0601/interview-collection/raw/master/images/MarkWord%E7%8A%B6%E6%80%81%E5%8F%98%E5%8C%96-2.png)



而 64 位虚拟机则于 32 位 的Mark Word 是有着一些差别的，Mark Word在 64位下所占空间为 64 bit。

![image-20220220160506742](https://gitee.com/yq0601/interview-collection/raw/master/images/64位 markword.png)

Mark Word 搞明白之后我们需要弄明白锁升级中的几个锁状态：无锁状态、偏向锁、轻量级锁、重量级锁。

**无锁状态**：顾名思义，没有加锁，这个没啥需要解释的。

**偏向锁**：当一个线程访问同步代码块并获取到锁时，会在对象头（Mark Word）和栈帧中的锁记录里存储锁偏向的线程 ID，该线程再次进入和退出同步代码块时不需要进行 CAS 操作来加锁和解锁，只需要简单的判断一下对象头（Mark Word）里是否存储者指向当前线程的偏向锁。如果是，表示该线程已经获取到了锁。如果不是，则需要再测试一下 Mark Word 中偏向锁的标志（偏向模式）是否设置为 1（表示当前是偏向锁），如果没有，则利用　CAS 竞争锁；如果是，则尝试使用 CAS 将对象头的偏向锁指向当前线程。



**偏向锁的撤销**：偏向锁使用了一种等待竞争才会释放锁的机制，所以偏向锁的撤销只有其他线程尝试竞争偏向锁的时候，只有偏向锁的线程才会释放锁。但需要注意的是：偏向锁的撤销需要一个全局安全点（所有线程都停止执行），它会先暂停持有偏向锁的线程，然后检查持有偏向锁的线程是否处于存活状态，如果该线程处于非活动状态，则立即结束偏向锁，根据锁对象目前是否处于被被锁的状态是否撤销偏向锁，撤销后标志位恢复到无锁状态或者轻量级锁状态。

偏向锁可以提高带用同步但无竞争的程序性能，但他同样是一把双刃剑，如果程序中大部分的锁都会被多个线程访问，那偏向锁可能会适得其反，所以需要根据程序的自身条件判断是否需要开启偏向锁。

偏向锁在 JDK 6、7 是自动开启的，如果你想关闭偏向锁，只需要将 JVM 的参数：-XX:-UseBiasedLocking=false，这样程序默认会进入轻量级锁状态。



**轻量级锁**：在线程即将进入同步代码块时，如果此同步对象没有被锁定（锁标志位为 ‘’01‘’），虚拟机首先会在当前线程的栈帧中建立一个名为锁记录（Lock Record）的空间，利用存储锁对象目前 Mark Word 的拷贝（官方称之为：Displaced Mark Word）。随后虚拟机将会使用 CAS 操作尝试将对象的 Mark Word 更新为指向 Lock Record 的指针。如果操作成功，说明当前线程已经拥有了这个对象的锁，并且对象的 Mark Word 锁标志将会修改为 “00”，表示此对象处于轻量级锁定状态，如果失败，那说明至少存在一个线程与当前线程竞争获取该对象的锁。虚拟机会优先检查对象的 Mark Word 是否指向当前线程的栈帧，如果是：说明线程已经拥有了这个对象的锁，直接执行代码块即可，否则说明对象已经被其他线程占用了，此时轻量级锁会膨胀成为重量级锁（轻量级的加锁）。

**轻量级的解锁**：如果对象的 Mark Word 还是指向线程的锁记录，则会使用 CAS 操作将当前对象的 Mark Word 和线程中复制的 Displaced Mark Word 进行替换，替换成功，说明同步过程顺利完成；替换失败，说明其他线程尝试获取过该锁，就需要释放锁的同时，唤醒被挂起的其他线程。

轻量级锁的不足：如果长时间得不到锁，CAS 操作会频繁的消耗 CPU。



**重量级锁**：这个大家也是非常熟悉，所有线程互斥，获取到锁的线程执行，其他线程阻塞被挂起，效率较差。



**锁升级的流程**

![锁升级流程](https://gitee.com/yq0601/interview-collection/raw/master/images/锁升级流程.jpg)

所以撤销偏向锁的时候标志位不一定会变成无锁，也有可能会升级为轻量级锁（存在锁竞争时）。



**偏向锁、轻量级锁、重量级锁的优缺点对比**

| 锁       | 优点                                     | 缺点/不足                                             | 应用场景                               |
| -------- | ---------------------------------------- | ----------------------------------------------------- | -------------------------------------- |
| 偏向锁   | 加锁/解锁不需要额外的消耗                | 如果线程存在锁竞争，会带来额外的锁撤销操作            | 只有一个线程访问的同步场景             |
| 轻量级锁 | 竞争的线程不会阻塞，提高了程序的响应速度 | 如果较长时间获取不到锁，CAS 自旋会大大增加 CPU 的消耗 | 追求响应时间、同步代码块执行速度非常快 |
| 重量级锁 | 线程竞争无需 CAS 自旋，不会消耗 CPU 资源 | 线程阻塞，响应缓慢                                    | 追求吞吐量、同步代码块执行较长         |



## spring面试精选

### Spring Framework有哪些核心组件？

spring-core：spring基础 API 模块，如资源管理，泛型处理等等。

spring-beans：spring Bean相关，如依赖查找，依赖注入等。

spring-aop：spring-AOP 处理，如动态代理，AOP字节码提升等。

spring-context：时间驱动，注解驱动，模块驱动等等。

spring-expression：Spring 表达式语言模块。

### 什么是IOC？

IOC可以理解为依赖反转，类似好莱坞原则，主要有以来查找和依赖注入实现。

Class A中用到了Class B的对象b，一般情况下，需要在A的代码中显式地用 new 创建 B 的对象。

采用依赖注入技术之后，A 的代码只需要定义一个 private 的B对象，不需要直接 new 来获得这个对象，而是通过相关的容器控制程序来将B对象在外部new出来并注入到A类里的引用中。而具体获取的方法、对象被获取时的状态由配置文件（如XML）来指定。

IoC也可以理解为把流程的控制从应用程序转移到框架之中。以前，应用程序掌握整个处理流程；现在，控制权转移到了框架，框架利用一个引擎驱动整个流程的执行，框架会以相应的形式提供一系列的扩展点，应用程序则通过定义扩展的方式实现对流程某个环节的定制，“框架Call应用”。基于MVC的web应用程序就是如此。

依赖查找和依赖注入的区别：两者的区别在于，前者是被动的接收对象，在类A的实例创建过程中即创建了依赖的B对象，通过类型或名称来判断将不同的对象注入到不同的属性中，而后者是主动索取相应类型的对象，获得依赖对象的时间也可以在代码中自由控制。

### 依赖查找和依赖注入的区别？

**依赖查找**：主动或者手动的依赖查找方式，通常还需要依赖容器或者标准 API 实现。

**依赖注入**：手动或者自动依赖绑定的方式，无需依赖特定的容器或者 API 。

**对比**

| 类型     | 依赖处理 | 实现便利性 | 代码侵入性 | API依赖性      | 可读性 |
| -------- | -------- | ---------- | ---------- | -------------- | ------ |
| 依赖查找 | 主动     | 繁琐       | 高侵入     | 依赖容器 API   | 良好   |
| 依赖注入 | 被动     | 便利       | 低侵入     | 不依赖容器 API | 一般   |



### Spring 作为IOC 容器有什么优势？

- IOC 管理，依赖查找和依赖注入。
- AOP 抽象
- 事务机制
- 事件机制
- SPI扩展
- 强大的第三方整合
- 易测试性
- 更好的面向对象



### 什么是Spring IOC 容器？

Spring 容器是 Spring 框架的核心。容器将创建对象，把它们连接在一起，配置它们，并管理他们的整个生命周期从创建到销毁。Spring 容器使用依赖注入（DI）来管理组成一个应用程序的组件。这些对象被称为 Spring Beans，我们将在下一章中进行讨论。

通过阅读配置元数据提供的指令，容器知道对哪些对象进行实例化，配置和组装。配置元数据可以通过 XML，Java 注释或 Java 代码来表示。下图是 Spring 如何工作的高级视图。 Spring IOC 容器利用 Java 的 POJO 类和配置元数据来生成完全配置和可执行的系统或应用程序。

**IOC 容器**具有依赖注入功能的容器，它可以创建对象，IOC 容器负责实例化、定位、配置应用程序中的对象及建立这些对象间的依赖。通常new一个实例，控制权由程序员控制，而"控制反转"是指new实例工作不由程序员来做而是交给Spring容器来做。在 Spring 中 BeanFactory 是IOC容器的实际代表者。



### BeanFactory 与 FactoryBean 的区别？

BeanFactory 是 IOC 的底层实现，给具体的 IOC 容器的实现提供了规范，顶层接口。

FactoryBean 是创建 Bean 的一种方式，帮助实现复杂的初始化逻辑。

FactoryBean 是个 Bean 在Spring 中，所有的 Bean 都是由 BeanFactory (也就是IOC容器)来进行管理的。但对 FactoryBean 而言，这个 Bean 不是简单的Bean，而是一个能生产或者修饰对象生成的工厂 Bean,它的实现与设计模式中的工厂模式和修饰器模式类似 。

### 如何注册一个Spring Bean？

通过BeanDefinition 和外部对象来注册。

### 什么是Spring BeanDefinition？

**定义**：BeanDefinition 是定义 Bean 的配置元信息接口，Spring根据 BeanDefinition 来定义 Bean 对象，简单说就是对 Bea n信息的定义

- 描述一个 bean 的全部信息，比如他的class类型、Bean 的作用域、是否懒加载…
- spring中每一个被扫描到的bean都会生成一个BeanDefinition。
- BeanDefinition的主要作用是为了在只解析一次类的情况下，最大程度的拿到这类的信息。防止重复解析导致效率变低。
- spring采用ASM（字节码解析的工具）技术去得到BeanDefinition。

**BeanDefinition中的常用属性**

- beanClass：表示 Bean 类型，未加载类的时候存放Bean的名字，加载类后存放Bean的class信息。
- scope：表示 Bean 的作用域，一般值为单例或者原型。
- lazyInit：表示 Bean 是否是懒加载。
- initMethodName：Bean 初始化需要执行的方法。
- destroyMethodName：Bean 销毁时要执行的方法。
- factoryBeanName：创建当前 Bean 的工厂。



### BeanFactory.getBean 是否线程安全？

是线程安全的，通过互斥锁 **synchronized** 保证线程安全。

### 有多少种依赖注入的方式？

1. 构造器注入
2. Setter注入
3. 字段注入
4. 方法注入
5. 接口回调注入



### 谈谈你对构造器注入和 setter 注入的理解

使用构造器注入需要将依赖作为构造函数的参数, 由于构造函数必须被调用, 因此强制的保证了依赖的初始化, 并且也将代码的依赖管理与 Spring 解耦, 实现了容器无关。

Setter 注入, setter 是否被调用具有不确定性, 因此对象实例可能永远不会得到依赖的组件. 唯一的方式是将`@Autowired `或`@Inject` 等注解添加到`setter` 上, 同时也打破了容器无关性.

两种依赖注入的方式都可以使用，如果是必须注入的话，推荐使用构造器注入，setter注入用于可选依赖



### Spring 依赖注入的来源有哪些？

1. 配置文件：可以通过 XML、Java 配置或者注解等方式来定义 Bean 的属性和依赖关系，Spring 在运行时根据配置文件中的定义来创建 Bean，并将依赖注入到相应的属性中。
2. 自动装配：Spring 可以自动扫描应用程序中的类，并自动装配相应的 Bean。自动装配可以通过 XML、Java 配置或者注解等方式来实现，可以根据类型、名称、注解等多种方式来匹配 Bean（通过BeanDefinition管理）
3. 编程方式：Spring 提供了一系列 API 来创建和管理 Bean，可以在代码中通过编程方式来创建 Bean，并将依赖注入到相应的属性中。
4. AOP 代理：Spring 通过 AOP 代理来实现依赖注入。在运行时，Spring 会动态地生成代理类，并将依赖注入到代理类中，然后将代理类返回给调用方。
5. 服务定位：Spring 还提供了服务定位机制，可以通过 JNDI、EJB 或者其他方式来查找并获取 Bean，然后将依赖注入到相应的属性中。

总的来说，Spring 依赖注入的来源非常灵活，可以通过各种方式来定义和获取 Bean，以满足不同的应用场景。

### 依赖查找和依赖注入的依赖来源是否相同？                                                                                                                                         

依赖查找和依赖注入都涉及到组件之间的依赖关系，但它们的依赖来源是不同的。

依赖查找是指组件直接从容器中获取其所依赖的其他组件的实例。组件需要明确知道其所依赖的组件的类型和名称，然后通过容器提供的 API 进行查找。

依赖注入则是将组件所依赖的其他组件实例通过容器主动注入到组件中，组件本身不需要直接从容器中获取所依赖的组件。依赖注入的方式有多种，比如构造函数注入、属性注入和方法注入等。

因此，依赖查找和依赖注入的依赖来源不同，依赖查找是组件自己主动从容器中获取依赖的组件，而依赖注入是容器将依赖的组件主动注入到组件中。



### 单例对象能在 IOC 容器启动后注册吗？

在大多数情况下，单例对象在IOC容器启动后注册是不可能的，因为IOC容器在启动时会立即实例化所有的单例bean并将其注册到容器中。如果您尝试在IOC容器启动后注册单例bean，它们将不会被容器所知道，也不会被容器管理。

但是，有些IOC容器可能支持在容器运行时注册bean。例如，Spring Framework提供了一种动态注册bean的机制，可以在运行时向IOC容器中添加新的bean定义，包括单例bean。您可以使用ConfigurableApplicationContext接口的registerBean方法来注册bean定义，这将导致IOC容器实例化并管理该bean。

总的来说，虽然有些IOC容器支持在容器运行时注册bean，但在IOC容器启动后注册单例bean是一个不常见的场景，因为通常单例bean应该在IOC容器启动时就被注册和管理，以确保它们能够正确地与其他bean进行交互。



### Spring 内建的 Bean 作用域有几种？

1. Singleton：单例模式，每个 Spring 容器中只会存在一个该 Bean 对象实例，默认作用域。
2. Prototype：原型模式，每次从容器中获取该 Bean 时都会创建一个新的对象实例。
3. Request：对于每次 HTTP 请求都会创建一个新的 Bean 实例，在该请求内部共享该 Bean 实例。
4. Session：对于每个 HTTP Session 都会创建一个新的 Bean 实例，在该 Session 内部共享该 Bean 实例。
5. Global Session：对于每个全局的 HTTP Session 都会创建一个新的 Bean 实例，在该全局 Session 内部共享该 Bean 实例，通常只在使用基于 Portlet 的 Web 应用时才会使用该作用域。

此外，还可以通过实现自定义的 Scope 接口来创建自定义的作用域。



### Singleton Bean 是否在一个应用中是唯一的？为什么？

是的，Singleton Bean在一个应用中是唯一的。

在Spring中，Bean是应用中的一个对象或组件，可以被其他组件依赖和使用。默认情况下，Spring容器会将Bean定义为Singleton作用域，这意味着每个Bean定义只会创建一个实例对象，并在整个应用中共享使用。

因此，当应用中需要使用某个Bean时，Spring容器会检查是否已经存在一个Singleton实例，如果存在则直接返回该实例，如果不存在则创建一个新的实例并返回。这样就确保了在整个应用中只有一个Singleton实例存在。

需要注意的是，虽然Singleton Bean是在整个应用中唯一的，但它并不是线程安全的，因此在多线程环境下使用Singleton Bean时需要进行适当的同步处理。此外，还有其他作用域的Bean可以在应用中创建多个实例，如Prototype、Session、Request等。



### application Bean 是否有有其他方法可以替代？

"Application Bean" 通常是指在 Spring Framework 中使用的一个特殊类型的组件，它通过 Spring 容器进行管理，并且在应用程序的生命周期内保持单例状态。

如果你不想使用 Application Bean，你可以使用其他类型的 Spring 组件来实现相同的功能。以下是一些可能的替代方法：

1. 普通的 Bean：可以使用普通的 Spring Bean 来替代 Application Bean。普通的 Bean 可以使用 @Bean 注解或者 XML 配置文件来定义，并且可以通过构造函数注入或者 @Autowired 注解进行注入。与 Application Bean 不同，普通的 Bean 不保持单例状态，每次调用时都会创建一个新的实例。
2. 静态类：如果你不需要在应用程序的生命周期内保持单例状态，可以考虑使用静态类来实现相同的功能。静态类是在应用程序启动时加载的，因此可以通过静态方法访问。
3. 工具类：如果你只需要一些静态方法来实现一些功能，可以考虑使用工具类。工具类是一个包含一些静态方法的类，通常不需要被实例化。你可以在任何地方调用这些方法，而不需要创建一个实例。

总的来说，"Application Bean" 是一种非常有用的组件类型，它提供了在整个应用程序中共享状态的能力。但是，如果你不需要这种功能，可以考虑使用其他类型的组件来替代它。



### BeanPostProcessor 的使用场景有哪些？

BeanPostProcessor 是 Spring 框架中的一个扩展点，它可以在 Spring 容器实例化、依赖注入、初始化完一个 Bean 后对该 Bean 进行额外的处理。

BeanPostProcessor 可以用于以下场景：

1. 自定义 Bean 初始化逻辑：使用 BeanPostProcessor 可以自定义 Bean 的初始化逻辑，例如对 Bean 进行一些特定的校验、修改 Bean 的属性等。
2. AOP 代理对象的创建：使用 BeanPostProcessor 可以对特定的 Bean 进行 AOP 的代理，例如在 Bean 初始化后，使用代理对象增强该 Bean 的某些方法。
3. 自定义 Bean 注册逻辑：使用 BeanPostProcessor 可以对 Bean 的注册过程进行控制，例如动态地将某些 Bean 注册到容器中。
4. 注解处理器：使用 BeanPostProcessor 可以自定义注解处理器，例如自定义一个 @MyAnnotation 注解，在 BeanPostProcessor 中扫描所有被该注解标记的 Bean 并进行处理。

需要注意的是，BeanPostProcessor 对于 Spring 容器中所有的 Bean 都会生效，因此在实现时需要注意只对需要处理的 Bean 进行适当的逻辑处理，以避免不必要的性能消耗。



### BeanFactoryPostProcessor 与 BeanPostProcessor 有什么区别？



### BeanFactory 是怎样处理 Bean 的生命周期？



### Spring 内建 XML Schema 有哪些？



### Spring 配置原信息具体有哪些？



### Extensible XML authoring 的缺点？



### java标准资源管理的扩展步骤是什么？



### Spring 有哪些 MessageSource 内建实现？



### Spring 校验接口是哪个？



### Spring 有哪些校验核心组件？



### Spring 数据绑定的 API 是什么？



### BeanWrapper 和 JavaBeans 之间的关系？



### DataBinder 是怎么完成属性类型转换的？



### Spring 类型转换的实现有哪些？



### Spring 类型转换器的接口有哪些？



### TypeDescriptor 是如何处理泛型的？



### java 泛型擦写发生在什么时期？



### 请说明 ResolvableType 的设计优势？



### Spring 事件核心组件/接口有哪些？



### Spring 同步和异步事件处理的使用场景分别是什么？



### @EventLiIstener 的工作原理是什么？



### Spring 的模式注解有哪些？



### @PropertySource的工作原理是什么？



### 简单介绍一下 Spring Environment 接口？



### 如何控制 PropertySource 的优先级？



###  请简单说一下 Environment 完整的生命周期？



### Spring 应用上下文生命周期的执行动作是什么？



### 请简单介绍一下 @Bean 的处理流程？



### BeanFactory 是如何处理循环依赖的？



###  



## springboot进阶篇

### 什么是Springboot ？ 与传统的web项目有什么却别或者优势？



### springboot自动装配原理

